//
//  Constants.swift
//  ShaeFoodDairy
//
//  Created by Bilal Saeed on 3/17/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

let kApplicationWindow = Utility.getAppDelegate()!.window
let kGoogleMapsAPI = "AIzaSyDOm8ZLmF-Nl5OEWlKafBBD8s3Ze3pFwjA"
let kErrorMessage = "There was a problem. Please try again."

struct APIRoutes {
    static let baseUrl = "https://krafty-backend.codesorbit.com"
    static let monthlyPurchase = "https://app.ph360.me/mobile/purchase?type=MONTHLY"
    static let getServicesList = "/api/customer/service/listServices"
    static let create = "/api/driver/user/create"
    static let verifyPin = "/api/driver/user/verifyPin"
    static let resendVerificationPin = "/api/driver/user/resendVerificationPin"
    static let signIn = "/api/authorization/auth/driverlogin"
    static let profileImageUrl = "\(APIRoutes.baseUrl)/api/driver/driverImages"
    static let customerProfileImageUrl = "\(APIRoutes.baseUrl)/api/customer/customerImages"
    static let forgotPassword = "/api/authorization/auth/forgotPassword"
    static let updatePassword = "/api/authorization/auth/updatePassword"
    static let changePassword = "/api/authorization/auth/changePassword"
    static let setNotificationStatus = "/api/driver/setting/setNotificationStatus"
    static let getNotificationStatus = "/api/driver/setting/getNotificationStatus"
    static let getUserDetials = "/api/driver/user/getDriverById"
    static let updateProfile = "/api/driver/user/update"
    static let registerComplaint = "/api/driver/complain/registerComplain"
    static let updateLocation = "/api/driver/user/updateLocation"
    static let getCompletedOrders = "/api/driver/home/getCompletedJobs"
    static let getProceedingOrders = "/api/driver/home/getPendingOrProceedingJobs"
    static let logout = "/api/authorization/auth/driverLogout"
    static let customerImageUrl = "\(APIRoutes.baseUrl)/api/customer/customerImage"
    static let socketEndPoint = "wss://krafty-backend.codesorbit.com"
    static let orderImages = "\(APIRoutes.baseUrl)/api/customer/orderImage/"
    static let acceptJob = "/api/driver/home/acceptJob"
    static let chatHistory = "/api/customer/order/getOrderChatHistory?order_id="
    static let driverArrived = "/api/driver/home/driverArrived"
    static let jobFinished = "/api/driver/home/orderFinish"
    static let cancelJob = "/api/driver/home/orderCancel"
    static let startJob = "/api/driver/home/startJob"
    static let addReview = "/api/driver/home/rateCustomer"
    static let getCurrentOrders = "/api/driver/order/getCurrentOrders"
    static let getOrdersHistory = "/api/driver/order/getOrderHistory"
    static let getOrderDetails = "/api/driver/order/getOrderDetail"
    static let orderImageUrl = "\(APIRoutes.baseUrl)/api/customer/orderImage"
    static let updateFCMToken = "/api/driver/user/updateFcmToken"
    static let getNotifications = "/api/driver/notification/getNotifications"
    static let clearNotifications = "/api/driver/notification/clearNotication"
    static let getDriverRatings = "/api/driver/setting/getRating"
    static let readNotification = "/api/driver/notification/readNotication"
    static let getAllOrders = "/api/driver/order/getAllOrders"
}
