//
//  Date.swift
//  Krafty-Vendor
//
//  Created by Mian Faizan Nasir on 7/15/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation

extension Date {
    var month: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM"
        return dateFormatter.string(from: self)
    }
    
    var year: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        return dateFormatter.string(from: self)
    }
}
