
import UIKit
import Alamofire
import ObjectMapper

class Connectivity {
    
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

let APIClientDefaultTimeOut = 40.0

class APIClient: APIClientHandler {
    
    fileprivate var clientDateFormatter: DateFormatter
    var isConnectedToNetwork: Bool?
    
    static var shared: APIClient = {
        let baseURL = URL(fileURLWithPath: APIRoutes.baseUrl)
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = APIClientDefaultTimeOut
        
        let instance = APIClient(baseURL: baseURL, configuration: configuration)
        
        return instance
    }()
    
    // MARK: - init methods
    
    override init(baseURL: URL, configuration: URLSessionConfiguration, delegate: SessionDelegate = SessionDelegate(), serverTrustPolicyManager: ServerTrustPolicyManager? = nil) {
        clientDateFormatter = DateFormatter()
        
        super.init(baseURL: baseURL, configuration: configuration, delegate: delegate, serverTrustPolicyManager: serverTrustPolicyManager)
        
        //        clientDateFormatter.timeZone = NSTimeZone(name: "UTC")
        clientDateFormatter.dateFormat = "yyyy-MM-dd" // Change it to desired date format to be used in All Apis
    }
    
    
    // MARK: Helper methods
    
    func apiClientDateFormatter() -> DateFormatter {
        return clientDateFormatter.copy() as! DateFormatter
    }
    
    fileprivate func normalizeString(_ value: AnyObject?) -> String {
        return value == nil ? "" : value as! String
    }
    
    fileprivate func normalizeDate(_ date: Date?) -> String {
        return date == nil ? "" : clientDateFormatter.string(from: date!)
    }
    
    var isConnectedToInternet: Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    func getUrlFromParam(apiUrl: String, params: [String: AnyObject]) -> String {
        var url = apiUrl + "?"
        
        for (key, value) in params {
            url = url + key + "=" + "\(value)&"
        }
        url.removeLast()
        return url
    }
    
    
    //MARK: - SignInViewController
    @discardableResult
    func signIn(email: String, password: String,  _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let headers = ["email": email, "password": password]
           return sendRequest(APIRoutes.signIn, parameters: nil, httpMethod: .post, headers:headers  , completionBlock: completionBlock)
    }
    
    @discardableResult
    func updateFCMToken(fcmToken:String, _ completionBlock:@escaping APIClientCompletionHandler)->Request {
        let tokenString = "bearer " + (User.shared.profile?.token ?? "")
        let params = ["fcm_token": fcmToken] as [String:AnyObject]
        return sendRequest(APIRoutes.updateFCMToken, parameters: params, httpMethod: .post, headers:["Content-Type":  "application/json", "Authorization": tokenString], completionBlock: completionBlock)
    }
    
    
    // MARK: - SignUpViewController
    @discardableResult
    func getServicesList( _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.getServicesList, parameters: nil, httpMethod: .get, headers:["Content-Type":  "application/json"]  , completionBlock: completionBlock)
    }
    
    @discardableResult
    func signUp(username:String, firstName: String, lastName: String, gender: String, age: String, email: String, password: String, phone: String, serviceType: String, etaNumber: String,  _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["username": username, "first_name": firstName, "last_name": lastName, "gender": gender, "age": age, "email":email, "password":password, "phone": phone, "service_type": serviceType, "eta_number": etaNumber] as [String:AnyObject]
        return sendRequest(APIRoutes.create, parameters: params, httpMethod: .post, headers:["Content-Type":  "application/json"]  , completionBlock: completionBlock)
    }
    
    
    //MARK: - CodeVerificationViewController
    @discardableResult
    func verifyPin(email: String, pin: String,  _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["email": email, "pin": pin] as [String:AnyObject]
           return sendRequest(APIRoutes.verifyPin, parameters: params, httpMethod: .post, headers:["Content-Type":  "application/json"]  , completionBlock: completionBlock)
    }
    
    @discardableResult
    func resendVerificationPin(email: String,  _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["email": email] as [String:AnyObject]
           return sendRequest(APIRoutes.resendVerificationPin, parameters: params, httpMethod: .post, headers:["Content-Type":  "application/json"]  , completionBlock: completionBlock)
    }
    
    //MARK: - ForgotPasswordViewController
    @discardableResult
    func forgotPassword(email: String,  _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["user_type": "2", "email": email] as [String:AnyObject]
           return sendRequest(APIRoutes.forgotPassword, parameters: params, httpMethod: .post, headers:["Content-Type":  "application/json"]  , completionBlock: completionBlock)
    }
    
    
    //MARK: - ChangePasswordViewController
    @discardableResult
    func updatePassword(token: String, password: String,  _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["user_type": "2", "password": password] as [String:AnyObject]
           return sendRequest(APIRoutes.updatePassword, parameters: params, httpMethod: .post, headers:["Content-Type":  "application/json", "Authorization": "bearer \(token)"]  , completionBlock: completionBlock)
    }
    
    @discardableResult
    func changePassword(oldPassword: String, newPassword: String,  _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["user_type": "2", "old_password": oldPassword, "new_password": newPassword] as [String:AnyObject]
        return sendRequest(APIRoutes.changePassword, parameters: params, httpMethod: .post, headers:["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"]  , completionBlock: completionBlock)
    }
    
    //MARK: - SettingsViewController
    @discardableResult
    func setNotificationStatus(status: Int,  _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["status": status] as [String:AnyObject]
        return sendRequest(APIRoutes.setNotificationStatus, parameters: params, httpMethod: .post, headers:["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"]  , completionBlock: completionBlock)
    }
    
    func getNotificationStatus( _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.getNotificationStatus, parameters: nil, httpMethod: .get, headers:["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"]  , completionBlock: completionBlock)
    }
    
    
    //MARK: - MyOrderViewController
    @discardableResult
    func getCurrentOrders( _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.getCurrentOrders, parameters: nil, httpMethod: .get, headers:["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"]  , completionBlock: completionBlock)
    }
    
    @discardableResult
    func getOrdersHistory( _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.getOrdersHistory, parameters: nil, httpMethod: .get, headers:["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"]  , completionBlock: completionBlock)
    }
    
    
    //MARK: - OrderHistoryViewController
    @discardableResult
    func getOrderDetails(orderId: Int,  _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["order_id": orderId] as [String:AnyObject]
        return sendRequest(APIRoutes.getOrderDetails, parameters: params, httpMethod: .get, headers:["Content-Type":  "application/json", "Authorization": "bearer " + User.shared.profile!.token]  , completionBlock: completionBlock)
    }
    
    
    //MARK: - ComplaintViewController
    @discardableResult
    func registerComplaint(orderId: Int, customerId: Int, description: String,  _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["order_id": orderId, "customer_id": customerId, "description": description] as [String:AnyObject]
        return sendRequest(APIRoutes.registerComplaint, parameters: params, httpMethod: .post, headers:["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"]  , completionBlock: completionBlock)
    }
    
    
    //MARK: - SideMenuViewController
    @discardableResult
    func logout( _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.logout, parameters: nil, httpMethod: .post, headers:["Content-Type":  "application/json", "Authorization": "bearer " + User.shared.profile!.token]  , completionBlock: completionBlock)
    }
    
    
    // MARK: - EditProfileViewController
    @discardableResult
    func getUserDetails(userId: Int,  _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["id": userId] as [String:AnyObject]
        return sendRequest(APIRoutes.getUserDetials, parameters: params, httpMethod: .get, headers:["Content-Type":  "application/json", "Authorization": "bearer " + (User.shared.profile?.token ?? "")]  , completionBlock: completionBlock)
    }
    
    func updateProfileData(profile: ProfileData, profileImage: UIImage?, completion: @escaping APIClientCompletionHandler) {
        
        let tokenString = "bearer " + (User.shared.profile?.token ?? "")
        let header: HTTPHeaders = ["Content-Type":  "application/x-www-form-urlencoded", "Authorization":tokenString]
        
        let parameters = ["id": String(profile.id), "username": profile.userName, "first_name": profile.firstName, "last_name": profile.lastName, "email": profile.email]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: .utf8)!, withName: key)
            }
            if profileImage != nil {
                //let rotatedImage = profileImage?.rotate(radians: .pi)
                multipartFormData.append((profileImage?.jpeg(.medium))!, withName: "image", fileName: "a66.jpg", mimeType: "image/png")
            }
        }, usingThreshold: 0, to: APIRoutes.baseUrl + APIRoutes.updateProfile, method: .post, headers: header, encodingCompletion: { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _ , _):
                upload.uploadProgress(closure: { (progress) in
                    print(progress)
                })
                upload.responseJSON { response in
                    switch response.result {
                    case .success:
                        self.showRequestDetailForSuccess(responseObject: response)
                        completion(response.result.value as AnyObject?, nil, 0)
                        break
                    case .failure(let error):
                        self.showRequestDetailForFailure(responseObject: response)
                        completion(nil, error as NSError?, 0)
                        break
                    }
                }
            case .failure:
                break
            }
        })
    }
    
    
    //MARK: - MapsViewController
    @discardableResult
    func updateLocation(location: String, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["location": location] as [String:AnyObject]
        return sendRequest(APIRoutes.updateLocation, parameters: params, httpMethod: .post, headers:["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"]  , completionBlock: completionBlock)
    }
    
    @discardableResult
    func getCompletedOrders( _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.getCompletedOrders, parameters: nil, httpMethod: .get, headers:["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"]  , completionBlock: completionBlock)
    }
    
    @discardableResult
    func getProceedingOrders( _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.getProceedingOrders, parameters: nil, httpMethod: .get, headers:["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"]  , completionBlock: completionBlock)
    }
    
    @discardableResult
    func acceptJob(orderId: Int, customerId: Int, _ completion: @escaping APIClientCompletionHandler) -> Request {
        let params = ["order_id": orderId, "customer_id": customerId] as [String: AnyObject]
        return sendRequest(APIRoutes.acceptJob, parameters: params, httpMethod: .post, headers: ["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"], completionBlock: completion)
    }
    
    @discardableResult
    func chatHistory(orderId: Int, _ completion: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.chatHistory + "\(orderId)", parameters: nil, httpMethod: .get, headers: ["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"], completionBlock: completion)
    }
    
    @discardableResult
    func driverArrived(orderId: Int, customerId: Int, _ completion: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.driverArrived, parameters: ["order_id" : orderId, "customer_id": customerId] as [String: AnyObject], httpMethod: .post, headers: ["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"], completionBlock: completion)
    }
    
    @discardableResult
    func jobFinished(orderId: Int, customerId: Int, _ completion: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.jobFinished, parameters: ["order_id" : orderId, "customer_id": customerId] as [String: AnyObject], httpMethod: .post, headers: ["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"], completionBlock: completion)
    }
    
    @discardableResult
    func cancelOrder(orderId: Int, location: String, _ completion: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.cancelJob, parameters: ["order_id" : orderId, "location": location] as [String: AnyObject], httpMethod: .post, headers: ["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"], completionBlock: completion)
    }
    
    @discardableResult
    func startJob(orderId: Int, _ completion: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.startJob, parameters: ["order_id" : orderId] as [String: AnyObject], httpMethod: .post, headers: ["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"], completionBlock: completion)
    }
    
    @discardableResult
    func addReview(customerId: Int, orderId: Int, perfromance: String, stars: Int, message: String, completion: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.addReview, parameters: ["order_id" : orderId, "customer_id" : customerId, "performance" : perfromance, "stars" : stars, "message" : message] as [String: AnyObject], httpMethod: .post, headers: ["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"], completionBlock: completion)
    }
    
    @discardableResult
    func getNotifications(completion: @escaping APIClientCompletionHandler) -> Request {
        
        return rawRequest(url: APIRoutes.baseUrl + APIRoutes.getNotifications, method: .get, parameters: nil, headers: ["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"], completionBlock: completion)
    }
    
    @discardableResult
    func clearNotifiaction(completion: @escaping APIClientCompletionHandler) -> Request {
        
        return sendRequest(APIRoutes.clearNotifications, parameters: nil, httpMethod: .post, headers: ["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"], completionBlock: completion)
    }
    
    @discardableResult
    func getRatings(completion: @escaping APIClientCompletionHandler) -> Request {
        
        return sendRequest(APIRoutes.getDriverRatings, parameters: nil, httpMethod: .get, headers: ["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"], completionBlock: completion)
    }
    
    @discardableResult
    func readNotifiaction(noificationID: Int, completion: @escaping APIClientCompletionHandler) -> Request {
        
        return sendRequest(APIRoutes.readNotification, parameters: ["notification_id": noificationID] as [String: AnyObject], httpMethod: .post, headers: ["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"], completionBlock: completion)
    }
    
    @discardableResult
    func getAllOrders( _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.getAllOrders, parameters: nil, httpMethod: .get, headers:["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"]  , completionBlock: completionBlock)
    }
    //
    //extension UIImage {
    //    func rotate(radians: Float) -> UIImage? {
    //        var newSize = CGRect(origin: CGPoint.zero, size: self.size).applying(CGAffineTransform(rotationAngle: CGFloat(radians))).size
    //        // Trim off the extremely small float value to prevent core graphics from rounding it up
    //        newSize.width = floor(newSize.width)
    //        newSize.height = floor(newSize.height)
    //
    //        UIGraphicsBeginImageContextWithOptions(newSize, false, self.scale)
    //        let context = UIGraphicsGetCurrentContext()!
    //
    //        // Move origin to middle
    //        context.translateBy(x: newSize.width/2, y: newSize.height/2)
    //        // Rotate around middle
    //        context.rotate(by: CGFloat(radians))
    //        // Draw the image at its center
    //        self.draw(in: CGRect(x: -self.size.width/2, y: -self.size.height/2, width: self.size.width, height: self.size.height))
    //
    //        let newImage = UIGraphicsGetImageFromCurrentImageContext()
    //        UIGraphicsEndImageContext()
    //
    //        return newImage
    //    }
}

