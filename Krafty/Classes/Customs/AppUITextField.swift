//
//  AppUITextField.swift
//  MissGold
//
//  Created by Bilal Saeed on 12/2/19.
//  Copyright © 2019 Bilal Saeed. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SDWebImage


class AppUITextField: SkyFloatingLabelTextField {


    //MARK: - Variables
    @IBInspectable var useBoldFont = false
    var padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    
    
    //MARK: - SkyFloatingLabelTextField Methods
    override func layoutSubviews() {
        super.layoutSubviews()
        self.backgroundColor = #colorLiteral(red: 0.9647058824, green: 0.9647058824, blue: 0.9647058824, alpha: 1)
        self.textColor = #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1)
        self.selectedTitleColor = #colorLiteral(red: 0.5868938565, green: 0.929741919, blue: 0.5990998149, alpha: 1)
        self.selectedLineColor = .clear
        self.titleColor = #colorLiteral(red: 0.5868938565, green: 0.929741919, blue: 0.5990998149, alpha: 1)
        self.lineColor = .clear
        self.placeholderColor = #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 0.5888259243)
        self.titleFormatter = { text in return text}
        self.isEnabled = true
        
        if useBoldFont {
            self.font = UIFont(name: "Poppins-Medium", size: 15) ?? UIFont.systemFont(ofSize: 14)
            
        } else {
            self.font = UIFont(name: "Poppins-Regular", size: 15) ?? UIFont.systemFont(ofSize: 14)
        }
        self.titleFont = UIFont(name: "Poppins-Regular", size: 12) ?? UIFont.systemFont(ofSize: 12)
    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func titleLabelRectForBounds(_ bounds: CGRect, editing: Bool) -> CGRect {
        if editing {
            padding.bottom = -10
            return CGRect(x: 0, y: -10, width: bounds.size.width, height: titleHeight())
        }
        padding.bottom = 0
        return CGRect(x: 0, y: titleHeight(), width: bounds.size.width, height: titleHeight())
    }
    
    
    //MARK: - Public Methods
    func setLeftImage(image:UIImage, width:Int = 20) {
        self.leftViewMode = .always
        let uiView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 30))
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: 20))
        imageView.image = image
        imageView.contentMode = .scaleToFill
        uiView.addSubview(imageView)
        self.leftView = uiView
    }
    
    func setLeftImage(imageUrl: String) {
        self.leftViewMode = .always
        let uiView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 30))
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        imageView.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage())
        imageView.contentMode = .scaleToFill
        uiView.addSubview(imageView)
        self.leftView = uiView
    }
}
