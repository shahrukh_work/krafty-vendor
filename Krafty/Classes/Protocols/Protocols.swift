//
//  Protocols.swift
//  ShaeFoodDairy
//
//  Created by Mac on 17/03/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import UIKit

protocol JobPopupDelegate: AnyObject {
    func acceptJob (jobID: Int, customerID: Int)
    func rejectJob()
}

protocol DirectionViewDelegate {
    func arrived()
    func cancel()
    func sendTapped(message: String)
}

protocol OrderSuccessViewControllerDelegate {
    func continueSuccess()
}

protocol JobsTableViewCellDelegate {
    func proceedButtonTapped(itemIndex: Int)
}

protocol RegisterComplaintDelegate {
    func registerComplaint(orderId: Int, driverId: Int, contractorName: String)
}
