//
//  Utility.swift
//  Audtix
//
//  Created by Bilal Saeed on 9/14/19.
//  Copyright © 2019 Bilal Saeed. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire


struct NetworkingConnection {
  static let sharedInstance = NetworkReachabilityManager()!
  static var isConnectedToInternet:Bool {
      return self.sharedInstance.isReachable
    }
}

@objc class Utility: NSObject {
    
    class func getAppDelegate() -> AppDelegate? {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        return appDelegate
    }
    
    class func loginRootViewController () {
        let controller = SignInViewController()
        let navigationController = UINavigationController()
        navigationController.viewControllers = [controller]
        navigationController.navigationBar.isHidden = true
        kApplicationWindow?.rootViewController = navigationController
        kApplicationWindow?.makeKeyAndVisible()
    }
    
    class func setupHomeAsRootViewController() {
        let leftViewController = SideMenuViewController()
        let mainViewController = MapsViewController()
        let sideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController)
        sideMenuController.delegate = leftViewController
        sideMenuController.changeLeftViewWidth(UIScreen.main.bounds.width)
        let navigationController = UINavigationController()
        navigationController.viewControllers = [sideMenuController]
        navigationController.navigationBar.isHidden = true
        kApplicationWindow!.rootViewController = navigationController
        kApplicationWindow!.makeKeyAndVisible()
    }
    
    class func autoLogin () {
        
//        if DataManager.shared.getUser() == nil {
//            loginRootViewController()
//            
//        } else {
//           setupHomeAsRootViewController()
//        }
    }
    
    class func setPlaceHolderTextColor (_ textField: UITextField, _ text: String, _ color: UIColor) {
        textField.attributedPlaceholder = NSAttributedString(string: text,
        attributes: [NSAttributedString.Key.foregroundColor: color])
    }
    
    class func changeFontSizeRange (mainString: String, stringToChange: String) ->  NSMutableAttributedString {
        let font = UIFont.systemFont(ofSize: 11)
        let range = (mainString as NSString).range(of: stringToChange)
        
        let attribute = NSMutableAttributedString.init(string: mainString)
        attribute.addAttribute(NSAttributedString.Key.font, value: font , range: range)
        return attribute
    }
    
    class func changeFontStyleToBold (mainString: String, stringToChange: String) ->  NSMutableAttributedString {
        let font = UIFont(name: "SFProText-Bold", size: 15)!
        let range = (mainString as NSString).range(of: stringToChange)
        
        let attribute = NSMutableAttributedString.init(string: mainString)
        attribute.addAttribute(NSAttributedString.Key.font, value: font , range: range)
        return attribute
    }
    
    class func addTextFieldLeftViewImage(_ textField: UITextField, image: UIImage, width: Int, height: Int, leftPadding: Int, topPadding: Int) {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: width + leftPadding + 5, height: height + topPadding))
        let imageView = UIImageView(frame: CGRect(x: leftPadding, y: topPadding, width: width, height: height))
        imageView.image = image
        view.addSubview(imageView)
        
        textField.leftViewMode = .always
        textField.leftView = view
    }
    
    class  func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
    
    class func isPhoneNumberValid (value: String) -> Bool {
        let PHONE_REGEX = "^[0-9]{10}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    class func getScreenHeight() -> CGFloat {
        return UIScreen.main.bounds.height
    }
    
    class func getScreenWidth() -> CGFloat {
        return UIScreen.main.bounds.width
    }
    
    class func makeBlurImage(targetImageView:UIImageView?, alpha: CGFloat = 1) {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = targetImageView!.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
        blurEffectView.alpha = alpha
        targetImageView?.addSubview(blurEffectView)
    }
    
    class func removeBlurFromImage(targetImageView: UIImageView?) {
        
        let blurViews = targetImageView?.subviews.filter({ (view) -> Bool in
            view.isKind(of: UIVisualEffectView.self)
        })
        
        blurViews?.forEach({ (view) in
            view.removeFromSuperview()
        })
    }
    
    @objc class func showLoading(offSet: CGFloat = 0, isVisible: Bool = true) {
        
        if let _ = kApplicationWindow?.viewWithTag(9000) {
            return
        }

        let superView = UIView(frame: CGRect(x: 0, y: 0 - offSet, width: kApplicationWindow?.frame.width ?? 0.0, height: kApplicationWindow?.frame.height ?? 0.0))
        let activityIndicator = NVActivityIndicatorView(frame: CGRect(x: superView.frame.width/2 - 32.5, y: superView.frame.height/2 - 32.5, width: 65, height: 65))
        let iconImageView = UIImageView(frame: CGRect(x: superView.frame.width/2 - 32.5, y: superView.frame.height/2 - 32.5, width: 65, height: 65))
        //iconImageView.image = #imageLiteral(resourceName: "loaderLogo")
        
        if isVisible {
            superView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            activityIndicator.color = #colorLiteral(red: 0.564625144, green: 0.9315071702, blue: 0.5658783317, alpha: 1)
        
        } else {
            superView.backgroundColor = .clear
            activityIndicator.color = .clear
        }
        
        superView.tag = 9000
        activityIndicator.type = .circleStrokeSpin
        activityIndicator.startAnimating()
        superView.addSubview(iconImageView)
        superView.addSubview(activityIndicator)
        superView.bringSubviewToFront(activityIndicator)
        superView.bringSubviewToFront(iconImageView)
        kApplicationWindow?.addSubview(superView)
    }
    
    @objc class func hideLoading() {
        if let activityView = kApplicationWindow?.viewWithTag(9000) {
            activityView.removeFromSuperview()
        }
    }
    
    class func simpleDate (date : Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let localDate = dateFormatter.string(from: date)
        return localDate
    }
    
    class func changeDateFormate (dataInString : String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let localDate = dateFormatter.date(from: dataInString)
        
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "MMM dd, yyyy"
        if localDate != nil {
            return dateFormatter.string(from: localDate!)
            
        } else {
            return ""
        }
    }
    
    class func dataInEnglish (_ dataInString : String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        let localDate = dateFormatter.date(from: dataInString)
        
        let calendar = Calendar.current
        
        if localDate != nil {
            
            if calendar.isDateInYesterday(localDate!) {
                return "Yesterday"
            } else if calendar.isDateInToday(localDate!) {
                return "Today"
            } else {
                dateFormatter.dateStyle = .medium
                dateFormatter.timeStyle = .none
                dateFormatter.dateFormat = "MMM dd, yyyy"
                if localDate != nil {
                    return dateFormatter.string(from: localDate!)
                    
                } else {
                    return ""
                }
            }
        }
        return ""
    }
    
    class func convertDateFormatter(date: String, inputFormat: String, outputFormat: String) -> String {
        let dateFormatter = DateFormatter()
        //Example: "MMM, d, yyyy"
        dateFormatter.dateFormat = inputFormat //this your string date format
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let convertedDate = dateFormatter.date(from: date)
        guard dateFormatter.date(from: date) != nil else {
            //assert(false, "no date from string")
            return ""
        }
        //Example: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.dateFormat =  outputFormat //this is the format you want the date to be converted in
        dateFormatter.timeZone = TimeZone.current
        let timeStamp = dateFormatter.string(from: convertedDate!)
        print(timeStamp)
        return timeStamp
    }
}

