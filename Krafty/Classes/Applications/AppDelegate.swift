//
//  AppDelegate.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/21/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces
import ObjectMapper
import netfox
import Firebase
import FirebaseMessaging

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        self.registerForPushNotification()
        NFX.sharedInstance().start()
        GMSPlacesClient.provideAPIKey(kGoogleMapsAPI)
        GMSServices.provideAPIKey(kGoogleMapsAPI)
        IQKeyboardManager.shared.enable = true
        
        NFX.sharedInstance().start()
        
        if UserDefaults.standard.string(forKey: "UserDefaults") != nil {
             let userDataString = UserDefaults.standard.string(forKey: "UserDefaults")
             User.shared.profile = Mapper<Profile>().map(JSONString: userDataString!)
             Utility.setupHomeAsRootViewController()
        
         } else {
            Utility.loginRootViewController()
         }
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
    }
    
    func registerForPushNotification(){
        UNUserNotificationCenter.current().delegate = self
        
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
        
                if error == nil{
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
            }

        } else {
            
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
}


//MARK: - MessagingDelegate
extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        User.shared.profile?.fcmToken = fcmToken
        
        PostApi.updateFCMToken(token: fcmToken) { (message) in
            print(message ?? "")
        }
    }
}


//MARK: - UNUserNotificationCenterDelegate
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print(notification)
        completionHandler([[.alert, .sound]])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        print("didReceive response")
        
        if let payLoad = (userInfo as? [String: AnyObject])?["job_details"] {
            print("payload received")
            
            if let payLoadString = payLoad as? String {
                
                if let data = Mapper<Job>().map(JSONString: payLoadString) {
                    
                    if let mapsController = User.shared.mapsController {
                        mapsController.currentJob = data
                        let controller = JobPopupViewController()
                        controller.data = data
                        controller.delegate = mapsController
                        controller.currentLocation = mapsController.currentLocation
                        controller.modalPresentationStyle = .overCurrentContext
                        mapsController.dismiss(animated: false, completion: nil)
                        mapsController.dismiss(animated: false, completion: nil)
                        mapsController.present(controller, animated: true, completion: nil)
                        
                    }
                }
            }
        }
        
        /*
         // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
                print("Handle push from background or closed\(response.notification.request.content.userInfo)")
                let dict = response.notification.request.content.userInfo["aps"] as! NSDictionary
                dict2 = (dict["alert"] as? [String:Any])!
                
                if let body = dict2["body"] as? String{
                    
                    if body.contains(":"){
                        var arr = body.split(separator: ":")
                        arr = arr[1].split(separator: ",")
                        last3 = String(arr[0].suffix(3))
                        
                    } else {
                        last3 = "ews"
                    }
                    
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let LoginViewController1 = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    self.window!.rootViewController = LoginViewController1
                }
                completionHandler()
         */
        completionHandler()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // 1. Convert device token to string
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        let token = tokenParts.joined()
        // 2. Print device token to use for PNs payloads
        print("Device Token: \(token)")
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        Messaging.messaging().appDidReceiveMessage(userInfo)
      }
}
