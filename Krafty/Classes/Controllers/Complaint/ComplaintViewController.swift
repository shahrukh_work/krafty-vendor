//
//  ComplaintViewController.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/23/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class ComplaintViewController: UIViewController {

    
    //MARK: - Outlets
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var orderIdTextField: UITextField!
    @IBOutlet weak var customerNameTextField: UITextField!
    @IBOutlet weak var complaintLabel: UILabel!
    
    
    //MARK: - Variables
    var orderId = -1
    var customerId = -1
    var customerName = ""
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
    //MARK: - View Setup
    private func setupView() {
        orderIdTextField.text = "\(orderId)"
        customerNameTextField.text = "\(customerName)"
        messageTextView.delegate = self
        messageTextView.text = "Your message"
        messageTextView.textColor = UIColor.lightGray
    }
    
    
    //MARK: - Actions
    @IBAction func launchButtonTapped(_ sender: Any) {
        
        if messageTextView.text == "" || messageTextView.text == "Your message" {
            //this clause indicates that the user focused the text view and didn't type anything or didn't even focus the textview
            self.showOkAlert("Please provide the complaint message.")
        
        } else {
            
            PostApi.registerComplaint(orderId: orderId, customerId: customerId, description: messageTextView.text) { (data, error, message) in
                
                if error == nil {
                    self.messageTextView.text = ""
                    self.messageTextView.resignFirstResponder()
                    self.showOkAlert("Your complaint has been registered.")
                
                } else {
                    self.showOkAlert("There was an error. Please try again.")
                }
            }
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}


//MARK: - TextViewDelegate
extension ComplaintViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Your message"
            textView.textColor = UIColor.lightGray
            
            UIView.animate(withDuration: 0.3) {
                self.complaintLabel.alpha = 0
            }
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        UIView.animate(withDuration: 0.3) {
            self.complaintLabel.alpha = 1
        }
    }
}
