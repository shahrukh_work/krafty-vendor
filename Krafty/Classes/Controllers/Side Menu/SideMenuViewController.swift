//
//  SideMenuViewController.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/27/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class SideMenuViewController: UIViewController {

    
    //MARK: - Outlets
    @IBOutlet weak var sideViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var sideViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var sideViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    
    
    //MARK: - Variables
    let optionIcons: [UIImage] = [#imageLiteral(resourceName: "ic_home"),#imageLiteral(resourceName: "history"), #imageLiteral(resourceName: "question"), #imageLiteral(resourceName: "information"), #imageLiteral(resourceName: "ic_notification"), #imageLiteral(resourceName: "settings")]
    let options = ["Home", "My Orders", "FAQs", "Support Chat", "Notifications", "Settings"]
    
    
    //MARK: - ViewController  Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !(self.slideMenuController()?.isLeftHidden())! {
            self.shadowView.alpha = 1.0
        }
        
        userNameLabel.text = "\(User.shared.profile?.profileData?.firstName ?? "") \(User.shared.profile?.profileData?.lastName ?? "")"
        profileImageView.sd_setImage(with: URL(string: APIRoutes.profileImageUrl + (User.shared.profile?.profileData?.image ?? "")), placeholderImage: #imageLiteral(resourceName: "profilePlaceholder"))
    }
    
    
    //MARK: - View Setup
    private func setupView() {
        shadowView.layer.cornerRadius = 10
        shadowView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        tableView.delegate = self
        tableView.dataSource = self
        getDriverRatings()
    }
    
    
    //MARK: - Actions
    @IBAction func logoutButtonTapped(_ sender: Any) {
        
        PostApi.logout() { (data, error, message) in
            
            if error == nil {
                User.shared.socket.disconnect()
                User.shared.profile = nil
                UserDefaults.standard.removeObject(forKey: "UserDefaults")
                Utility.loginRootViewController()
                
            } else {
                self.showOkAlert(kErrorMessage)
            }
        }
    }
    
    @IBAction func crossButtonTapped(_ sender: Any) {
        slideMenuController()?.closeLeft()
    }
    
    
    //MARK: - Private Methods
    private func pushController(controller: UIViewController) {
        
        let rootController = self.navigationController?.viewControllers.first(where: { (viewController) -> Bool in
            return viewController == controller
        })
        
        if let cntrlr = rootController {
            self.navigationController?.popToViewController(cntrlr, animated: false)
            
        } else {
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }
    
    private func getDriverRatings() {
        Utility.showLoading()
        
        Rating.getRatings { (data, error, message) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = data {
                    self.ratingLabel.text = "\(data.stars)"
                }
            }
        }
    }
}


//MARK: - TableView Datasource & Delegate
extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(SideMenuTableViewCell.self, indexPath: indexPath)
        cell.selectionStyle = .none
        cell.configCell(image: optionIcons[indexPath.row], option: options[indexPath.row], itemIndex: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        
        case 0: // home
            self.closeLeft()
        
        case 1: // History
            let controller = MyOrderViewController()
            pushController(controller: controller)
            break
            
        case 3:
            let controller = ChatViewController()
            pushController(controller: controller)
            break
            
        case 4: // rating of the app
            let controller = NotificationsViewController()
            pushController(controller: controller)
            break
        
        case 5: // rating of the app
            let controller = SettingsViewController()
            pushController(controller: controller)
            break
            
        default: // settings
            let controller = FAQsViewController()
            pushController(controller: controller)
            break
        }
    }
}


//MARK: - SideMenuDelegate
extension SideMenuViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        UIView.animate(withDuration: 0.5, animations: {
            self.shadowView.alpha = 1.0
             self.sideViewWidthConstraint.constant = 33
            self.sideViewTopConstraint.constant = 70
            self.sideViewBottomConstraint.constant = 40
            self.view.layoutIfNeeded()
        })
    }
    
    func leftDidOpen() {
        self.shadowView.alpha = 1.0
        self.sideViewWidthConstraint.constant = 33
        self.sideViewTopConstraint.constant = 70
        self.sideViewBottomConstraint.constant = 40
    }

    func leftWillClose() {

        UIView.animate(withDuration: 0.5, animations: {
            self.sideViewWidthConstraint.constant = 220
            self.sideViewTopConstraint.constant = 0
            self.sideViewBottomConstraint.constant = 0
            self.shadowView.alpha = 0.0
            self.view.layoutIfNeeded()
        })
    }
    
    func leftDidClose() {
        self.shadowView.alpha = 0.0
        self.sideViewWidthConstraint.constant = 220
        self.sideViewTopConstraint.constant = 0
        self.sideViewBottomConstraint.constant = 0
    }
}
