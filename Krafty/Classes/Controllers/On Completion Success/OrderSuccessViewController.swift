//
//  OrderSuccessViewController.swift
//  Krafty-Vendor
//
//  Created by Mac on 30/04/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class OrderSuccessViewController: UIViewController {
    
    
    //MARK: - Outlet


    //MARK: - Variable
    var delegate: OrderSuccessViewControllerDelegate?
    

    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    //MARK : - Actions
    @IBAction func continuePressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        delegate?.continueSuccess()
    }
}
