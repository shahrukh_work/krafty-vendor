//
//  DirectionView.swift
//  Krafty
//
//  Created by Bilal Saeed on 4/28/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import ObjectMapper
import CoreLocation
import Alamofire
import SwiftyJSON


class DirectionView: UIView {
    
    
    //MARK: - Variables
    var view: UIView!
    var delegate: DirectionViewDelegate?
    var data: Job = Mapper<Job>().map(JSON: [:])!
    var currentLocation = CLLocationCoordinate2D()
    var chatData = Mapper<ChatHistory>().map(JSON: [:])!
    var distanceCheckValue = 500
    
    
    //MARK: - Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var mileView: UIView!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var arrivedView: UIView!
    @IBOutlet weak var milesLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var arrived: UIButton!
    @IBOutlet weak var personImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var messageTextViewHeightConstraint: NSLayoutConstraint!
    
    
    //MARK: - View Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    //MARK: - View Setup
    func setup() {
        backgroundColor = .clear
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
        setupView()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return nibView
    }
    
    func setupView() {
        messageTextView.delegate = self
        messageTextView.textColor = #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1)
        tableView.delegate = self
        tableView.dataSource = self
        containerView.cornerRadius = 9
        containerView.addShadow(offset: CGSize(width: 0, height: 0), color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.26), opacity: 1.0, radius: 0.5)
        containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
    
    
    //MARK: - Public Methods
    func incommingChat(chat: Chat) {
        self.chatData.chatHistory.append(chat)
        
        if let tableView = tableView {
            tableView.reloadData()
            
            if !chatData.chatHistory.isEmpty {
                tableView.scrollToRow(at: IndexPath(row: chatData.chatHistory.count - 1, section: 0), at: .bottom, animated: false)
            }
        }
    }
    
    
    func loadData() {
        
        Utility.showLoading()
        ChatHistory.loadChat(orderId: self.data.id) { (data, error) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = data {
                    self.chatData = data
                    self.tableView.reloadData()
                    
                    if !self.chatData.chatHistory.isEmpty {
                        self.tableView.scrollToRow(at: IndexPath(row: self.chatData.chatHistory.count - 1, section: 0), at: .bottom, animated: false)
                    }
                }
            }
        }
    }
    
    func updateUI() {
        
        if data.status == 1 {
            arrived.setTitle("Arrived", for: .normal)
            
        } else if data.status == 5 {
            arrived.setTitle("Finish", for: .normal)
        }
    }
    
    
    //MARK: - Private Methods
    func populateData() {
        
        if let url = URL(string: APIRoutes.customerProfileImageUrl + data.customerImage) {
            personImageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "ic_image"))
        }
        nameLabel.text = data.customerName
        addressLabel.text = data.orderAddress
    }
    
    
    func updateLocation() {
        let dest = CLLocationCoordinate2D(latitude: Double(data.lat) ?? 0.0, longitude: Double(data.long) ?? 0.0)
        calculateTimeDistance(src: currentLocation, dst: dest)
    }
    
    
    //MARK: - Private Methods
    private func calculateTimeDistance(src: CLLocationCoordinate2D, dst: CLLocationCoordinate2D){
        let origin = "\(src.latitude),\(src.longitude)"
        let destination = "\(dst.latitude),\(dst.longitude)"
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(kGoogleMapsAPI)"
        
        Alamofire.request(url).responseJSON { [weak self] response in
            
            do {
                let json = try JSON(data: response.data!)
                let route = json["routes"].arrayValue.first
                
                if let route = route {
                    let leg = route["legs"].arrayValue.first
                    
                    if let leg = leg {
                        let distance = leg["distance"].dictionaryObject?["text"]
                        let distanceValue = (leg["distance"].dictionaryObject?["value"] as? Int) ?? 0
                        
                        if let distance = distance as? String {
                            self?.milesLabel.text = "\(distance) away"
                        }
                        
                        if distanceValue <= self?.distanceCheckValue ?? 0 {
                            self?.arrived.isUserInteractionEnabled = true
                            self?.arrived.alpha = 1
                            
                        } else {
                            self?.arrived.isUserInteractionEnabled = false
                            self?.arrived.alpha = 0.5
                        }
                    }
                }
                
            } catch {
                Utility.hideLoading()
                print(error.localizedDescription)
            }
        }
        
        updateUI()
    }
    
    
    //MARK: - Actions
    @IBAction func sendAction(_ sender: Any) {
        
        if (messageTextView.text ?? "") == "" {
            return
        }
        
        let chat = Mapper<Chat>().map(JSON: [
            "id" : -1,
            "order_id" : data.id,
            "message": messageTextView.text ?? "",
            "send_by" : 2,
            "created_at" : "",
            "updated_at" : "",
        ])
        
        if let chat = chat {
            self.chatData.chatHistory.append(chat)
        }
        delegate?.sendTapped(message: messageTextView.text ?? "")
        messageTextView.text = ""
        
        self.tableView.reloadData()
        
        if !chatData.chatHistory.isEmpty {
            tableView.scrollToRow(at: IndexPath(row: chatData.chatHistory.count - 1, section: 0), at: .bottom, animated: false)
        }
    }
    
    @IBAction func arrivedAction(_ sender: Any) {
        delegate?.arrived()
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        delegate?.cancel()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}


//MARK: - TextView Delegate
extension DirectionView: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.textColor == #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1) {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == "" {
            messageTextViewHeightConstraint.constant = 40
            textView.text = "Type"
            textView.textColor = #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1)
        }
    }
    
    //change in text view height disturbs the name and rating components in the ui so no change in the text view height should be done
    //    func textViewDidChange(_ textView: UITextView) {
    //        let minTextViewHeight: CGFloat = 40
    //        let maxTextViewHeight: CGFloat = 80
    //
    //        var height = ceil(textView.contentSize.height) // ceil to avoid decimal
    //
    //        if (height < minTextViewHeight) {
    //            height = minTextViewHeight
    //        }
    //
    //        if (height > maxTextViewHeight) {
    //            height = maxTextViewHeight
    //        }
    //
    //        if height != messageTextViewHeightConstraint.constant {
    //            messageTextViewHeightConstraint.constant = height
    //            textView.setContentOffset(.zero, animated: false) // scroll to top to avoid "wrong contentOffset" artefact when line count changes
    //        }
    //    }
}


//MARK: - UITableViewDelegate & UITableViewDataSource
extension DirectionView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatData.chatHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let chat = chatData.chatHistory[indexPath.row]
        
        if chat.sendBy == 1 {
            let cell = tableView.register(IncomingMessageMapTableViewCell.self, indexPath: indexPath)
            cell.configCell(message: chat.message)
            return cell
        }
        
        let cell = tableView.register(OutgoingMessageMapTableViewCell.self, indexPath: indexPath)
        cell.configCell(message: chat.message)
        
        return cell
    }
}
