//
//  MapsViewController.swift
//  Krafty
//
//  Created by Bilal Saeed on 4/24/20.
//  Copyright © 2020 Bilal Saeed. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON
import IQKeyboardManagerSwift
import SocketIO
import ObjectMapper


class MapsViewController: UIViewController {
    
    
    //MARK: - Variables
    private let locationManager = CLLocationManager()
    var currentLocation = CLLocationCoordinate2D()
    var marker = GMSMarker()
    var jobPositionMarker = GMSMarker()
    var sourceView: UIView?
    let cardSize = [174, 341, 388, 564, 564, 104, 245]
    let findingServiceProviderView = DirectionView()
    let pulsator = Pulsator()
    let jobPulsator = Pulsator()
    var completedOrdersList = [Order]()
    var proceedingOrdersList = [Order]()
    var currentJob = Mapper<Job>().map(JSON: [:])!
    var isOnJob = false
    var manager:SocketManager!
    var timer: Timer!
    var jobTimer: Timer!
    var doOnce = true
    var myLocation: CLLocation?
    var isShowingCompleteOrders = false
    var isFirstTime = true
    
    //MARK: - Outlets
    @IBOutlet weak var mapsView: GMSMapView!
    @IBOutlet weak var tableViewContainer: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cardContainerView: UIView!
    @IBOutlet weak var viewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var screenTitleLabel: UILabel!
    @IBOutlet weak var completedJobsLabel: UILabel!
    @IBOutlet weak var pendingJobsLabel: UILabel!
    @IBOutlet weak var jobsView: UIView!
    @IBOutlet weak var sendMessageViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var sideMenuButton: UIButton!
    @IBOutlet weak var topNavigationHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var pendingLabel: UILabel!
    @IBOutlet weak var completedLabel: UILabel!
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        User.shared.mapsController = self
        setupSocks()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupView()
        locationSetup()
        registerKeyboardNotifications()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if isFirstTime {
            isFirstTime.toggle()
            slideExitInsert(view: findingServiceProviderView, height: 313, exitLeft: true)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.enable = true
        NotificationCenter.default.removeObserver(self)
    }
    
    
    //MARK: - Setup
    private func setupView() {
        getCompletedOrders()
        getProceedingOrders()
        findingServiceProviderView.delegate = self
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        
        tableViewContainer.isHidden = true
        cardContainerView.isHidden = true
        tableView.isHidden = true
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeUp.direction = .up
        self.cardContainerView.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeDown.direction = .down
        self.cardContainerView.addGestureRecognizer(swipeDown)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        setupCurrentLocationMark()
        
        if let styleURL = Bundle.main.url(forResource: "MapStyle", withExtension: "json") {
            
            do {
                mapsView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                
            } catch {
                print(error.localizedDescription)
            }
            
        } else {
            NSLog("Unable to find MapStyle.json")
        }
        
        timer = Timer.scheduledTimer(timeInterval: 30.0, target: self, selector: #selector(updateLocation), userInfo: nil, repeats: true)
        jobTimer = Timer.scheduledTimer(timeInterval: 6.0, target: self, selector: #selector(updateLocationCustomer), userInfo: nil, repeats: true)
        
        mapsView.delegate = self
    }
    
    func locationSetup() {
        locationManager.delegate = self
        
        switch(CLLocationManager.authorizationStatus()) {
            
        case .restricted, .denied:
            self.showOkAlertWithOKCompletionHandler("Application uses location data for navigations and maps. Kindly enable location services in order to use the application.") { ( _) in
                UIApplication.shared.open(NSURL(string:UIApplication.openSettingsURLString)! as URL)
            }
            
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.allowsBackgroundLocationUpdates = true
            locationManager.startUpdatingLocation()
            locationManager.startUpdatingHeading()
            
        case .notDetermined:
            locationManager.requestAlwaysAuthorization()
            
        default:
            break
        }
    }
    
    
    //MARK: - Selectors
    @objc func updateLocation() {
        let updatedLocation = "\(currentLocation.latitude),\(currentLocation.longitude)"
        
        PostApi.updateLocation(location: updatedLocation) { (data, error, message) in
            
            if error == nil {
                print("Location updated to \(updatedLocation)")
                
            } else {
                print("Failed to update the location")
            }
        }
    }
    
    @objc func updateLocationCustomer() {
        locationManager.startUpdatingLocation()
        //for updating the admin panel about driver's location
        User.shared.socket.emit("share-location", ["driver_id" : User.shared.profile?.profileData?.id ?? -1, "latitude" : currentLocation.latitude, "longitude" : currentLocation.longitude])
        
        if isOnJob {
            //for updating the customer app about driver's location
            proceedingOrdersList.forEach { (order) in
                User.shared.socket.emit("update-location", ["customer_id" : order.customerId, "latitude" : currentLocation.latitude, "longitude" : currentLocation.longitude])
            }
            let destination = CLLocationCoordinate2D(latitude: Double(currentJob.lat) ?? 0.0, longitude: Double(currentJob.long) ?? 0.0)
            draw(src: currentLocation, dst: destination)
        }
    }
    
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        
        if gesture.direction == .up {
            self.viewHeightConstraint.constant = 313
            self.findingServiceProviderView.mileView.isHidden = false
            self.findingServiceProviderView.arrivedView.isHidden = false
            self.findingServiceProviderView.messageView.isHidden = false
            self.findingServiceProviderView.tableView.isHidden = true
            
            UIView.animate(withDuration: 0.1) {
                self.view.layoutIfNeeded()
            }
            
        } else if gesture.direction == .down {
            self.topNavigationHeightConstraint.constant = 80
            self.viewHeightConstraint.constant = 110
            self.jobsView.isHidden = true
            
            self.sideMenuButton.isHidden = true
            self.backButton.isHidden = false
            self.notificationButton.isHidden = true
            self.titleLabel.isHidden = true
            
            self.findingServiceProviderView.mileView.isHidden = false
            self.findingServiceProviderView.arrivedView.isHidden = true
            self.findingServiceProviderView.messageView.isHidden = true
            
            UIView.animate(withDuration: 0.1) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    
    //MARK: - Actions
    @IBAction func hamburgerPressed(_ sender: Any) {
        //slideExitInsert(view: serviceSelectionView, height: cardSize[1])
    }
    
    @IBAction func sideMenuPressed(_ sender: Any) {
        self.openLeft()
    }
    
    @IBAction func notificationPressed(_ sender: Any) {
        let modalViewController = NotificationsViewController()
        self.navigationController?.pushViewController(modalViewController, animated: true)
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.hideShowViews(false)
        self.topNavigationHeightConstraint.constant = 120
        self.findingServiceProviderView.isHidden = true
        self.mapsView.clear()
        setupCurrentLocationMark()
        
        UIView.animate(withDuration: 0.1) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func completedPressed(_ sender: Any) {
        
        if completedOrdersList.isEmpty {
            self.showOkAlert("You have no completed jobs yet.")
            return
        }
        
        completedLabel.textColor = #colorLiteral(red: 0.5952221155, green: 0.6189013124, blue: 0.6646106839, alpha: 1)
        pendingLabel.textColor = #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1)
        tableViewContainer.isHidden.toggle()
        cardContainerView.isHidden = true
        tableView.isHidden.toggle()
        isShowingCompleteOrders = true
        findingServiceProviderView.isHidden = true
        setupCurrentLocationMark()
        tableView.reloadData()
    }
    
    @IBAction func pendingPressed(_ sender: Any) {
        
        if proceedingOrdersList.isEmpty {
            self.showOkAlert("You have no pending jobs yet.")
            return
        }
        
        completedLabel.textColor = #colorLiteral(red: 0.5952221155, green: 0.6189013124, blue: 0.6646106839, alpha: 1)
        pendingLabel.textColor = #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1)
        tableViewContainer.isHidden.toggle()
        cardContainerView.isHidden = true
        tableView.isHidden.toggle()
        isShowingCompleteOrders = false
        findingServiceProviderView.isHidden = true
        setupCurrentLocationMark()
        tableView.reloadData()
    }
    
    
    //MARK: - Private Methods
    private func getCompletedOrders() {
        
        Orders.getCompletedOrders() { (data, error, message) in
            
            if error == nil {
                self.completedOrdersList = data!.completedOrdersList
                self.completedJobsLabel.text = "\(self.completedOrdersList.count) Jobs"
                
            } else {
                print("Could not fetch completed orders")
            }
        }
    }
    
    private func getProceedingOrders() {
        
        Orders.getProceedingOrders { (data, error, message) in
            
            if error == nil {
                self.proceedingOrdersList = data!.proceedingOrdersList
                self.pendingJobsLabel.text = "\(self.proceedingOrdersList.count) Jobs"
                
            } else {
                print("Could not fetch proceeding orders")
            }
        }
    }
    
    func hideShowViews (_ isJobAccepted: Bool) {
        self.jobsView.isHidden = isJobAccepted
        self.notificationButton.isHidden = isJobAccepted
        self.backButton.isHidden = !isJobAccepted
        self.titleLabel.isHidden = isJobAccepted
        self.sideMenuButton.isHidden = isJobAccepted
    }
    
    private func draw(src: CLLocationCoordinate2D, dst: CLLocationCoordinate2D){
        let origin = "\(src.latitude),\(src.longitude)"
        let destination = "\(dst.latitude),\(dst.longitude)"
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(kGoogleMapsAPI)&alternatives=false"
        
        Alamofire.request(url).responseJSON { response in
            
            do {
                let json = try JSON(data: response.data!)
                let routes = json["routes"].arrayValue
                
                for route in routes {
                    let routeOverviewPolyline = route["overview_polyline"].dictionary
                    let points = routeOverviewPolyline?["points"]?.stringValue
                    let path = GMSPath.init(fromEncodedPath: points!)
                    
                    let polyline = GMSPolyline(path: path)
                    polyline.strokeColor = #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1)
                    polyline.strokeWidth = 2
                    polyline.map = self.mapsView
                }
                
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    private func slideExitInsert(view: UIView, height: Int, exitLeft: Bool = true) {
        let width = cardContainerView.frame.width
        
        var multiplier:CGFloat = 1.1
        
        if !exitLeft {
            multiplier = -1.1
        }
        
        let insertFrame = CGRect(x: multiplier * width, y: 0.0, width: width, height: CGFloat(height))
        
        if let exitView = cardContainerView.subviews.first {
            
            var y: CGFloat = 0.0
            let exitViewHeight = exitView.frame.height
            
            if exitViewHeight < CGFloat(height) {
                y = CGFloat(height) - exitViewHeight
                exitView.frame = CGRect(x: 0, y: y, width: width, height: exitViewHeight)
            }
            
            UIView.animate(withDuration: 0.4, animations: { [weak self] in
                self?.viewHeightConstraint.constant = CGFloat(height)
                
                exitView.frame = CGRect(x: -multiplier * width, y: y, width: width, height: exitViewHeight)
                
            }) { ( _) in
                exitView.removeFromSuperview()
            }
        }
        
        viewHeightConstraint.constant = CGFloat(height)
        view.frame = insertFrame
        view.layoutIfNeeded()
        cardContainerView.layoutIfNeeded()
        self.findingServiceProviderView.tableView.isHidden = true
        cardContainerView.addSubview(view)
        
        UIView.animate(withDuration: 0.4) {
            view.frame = CGRect(x: 0, y: 0.0, width: width, height: CGFloat(height))
        }
    }
    
    
    //MARK: - Sockets
    private func setupSocks() {
        manager = SocketManager(socketURL: URL(string: APIRoutes.socketEndPoint)!, config: [.log(true), .compress, .secure(true), .extraHeaders(["Authorization" : "Bearer \(User.shared.profile?.token ?? "")"])])
        User.shared.socket = manager.defaultSocket
        User.shared.socket.connect()
        
        User.shared.socket.on(clientEvent: .connect) { (data, error) in
            User.shared.socket.emit("join-driver", ["driver_id": "\(User.shared.profile?.profileData?.id ?? -1)"])
        }
        
        User.shared.socket.on(clientEvent: .error) { (data, ackEmitter) in
            print(data)
        }
        
        self.socksListenEvents()
    }
    
    private func socksListenEvents() {
        
        User.shared.socket.on("job-request") { (sockData, ackEmitter) in
            
            if let socketData = sockData.first {
                if let data = Mapper<Job>().map(JSONObject: socketData) {
                    self.currentJob = data
                    let controller = JobPopupViewController()
                    controller.data = data
                    controller.delegate = self
                    controller.currentLocation = self.currentLocation
                    controller.modalPresentationStyle = .overCurrentContext
                    self.topMostViewController().present(controller, animated: true, completion: nil)
                    
                } else {
                    self.showOkAlert("Something went wrong!.")
                }
            }
        }
        
        User.shared.socket.on("add-message") { (sockData, ackEmitter) in
            
            if let socketData = sockData.first {
                
                if let data = Mapper<Chat>().map(JSONObject: socketData) {
                    self.findingServiceProviderView.incommingChat(chat: data)
                    
                } else {
                    print("error parsing chat")
                }
            }
        }
        
        User.shared.socket.on("current-job") { [weak self] (sockData, ackEmitter) in
            
            if let socketData = sockData.first {
                
                if let data = Mapper<Job>().map(JSONObject: socketData) {
                    self?.currentJob = data
                    
                    IQKeyboardManager.shared.enableAutoToolbar = false
                    IQKeyboardManager.shared.enable = false
                    self?.registerKeyboardNotifications()
                    self?.cardContainerView.isHidden = false
                    self?.findingServiceProviderView.isHidden = false
                    self?.hideShowViews(true)
                    self?.isOnJob = true
                    self?.tableView.isHidden = true
                    
                    if self?.currentJob.status == -1 {
                        self?.currentJob.status = 1
                    }
                    if let currentJob = self?.currentJob, let currentLocation = self?.currentLocation {
                        self?.findingServiceProviderView.data = currentJob
                        self?.findingServiceProviderView.currentLocation = currentLocation
                    }
                    
                    self?.findingServiceProviderView.loadData()
                    self?.findingServiceProviderView.populateData()
                    self?.findingServiceProviderView.updateLocation()
                    self?.topNavigationHeightConstraint.constant = 80
                    
                    UIView.animate(withDuration: 0.1) {
                        self?.view.layoutIfNeeded()
                    }
                    
                    self?.getProceedingOrders()
                    self?.getCompletedOrders()
                    self?.setupCurrentLocationMark()
                }
            }
        }
        
        // if vendor started/accepted the job and customer cancelled it
        User.shared.socket.on("job-cancel") { (sockData, ackEmitter) in
            //TODO: - after requirements cleared by Taimur
            self.getProceedingOrders()
            self.backPressed("")
            //self.tableView.reloadData()
        }
    }
    
    private func startJob(jobID: Int) {
        
        PostApi.startJob(orderId: jobID) { [weak self] (error) in
            
            if error == nil {
                
                IQKeyboardManager.shared.enableAutoToolbar = false
                IQKeyboardManager.shared.enable = false
                self?.registerKeyboardNotifications()
                self?.cardContainerView.isHidden = false
                self?.findingServiceProviderView.isHidden = false
                self?.hideShowViews(true)
                self?.isOnJob = true
                self?.tableViewContainer.isHidden = true
                self?.tableView.isHidden = true
                
                if self?.currentJob.status == -1 {
                    self?.currentJob.status = 1
                }
                if let currentJob = self?.currentJob, let currentLocation = self?.currentLocation {
                    self?.findingServiceProviderView.data = currentJob
                    self?.findingServiceProviderView.currentLocation = currentLocation
                }
                
                self?.findingServiceProviderView.loadData()
                self?.findingServiceProviderView.populateData()
                self?.findingServiceProviderView.updateLocation()
                self?.topNavigationHeightConstraint.constant = 80
                
                UIView.animate(withDuration: 0.1) {
                    self?.view.layoutIfNeeded()
                }
                
                self?.getCompletedOrders()
                self?.getProceedingOrders()
                self?.setupCurrentLocationMark()
            }
        }
    }
    
    private func setupCurrentLocationMark() {
        
        let iconView = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
        var image = UIImageView(frame: CGRect(x: iconView.center.x - 10, y:  iconView.center.y + 20, width: 20,height: 20))
        image.image = #imageLiteral(resourceName: "ic_start")
        
        if !isOnJob {
            image = UIImageView(frame: CGRect(x: iconView.center.x - 10, y:  iconView.center.y - 5, width: 30,height: 40))
            image.image = #imageLiteral(resourceName: "ic_locaton")
        }
        iconView.addSubview(image)
        marker.iconView = iconView
        marker.map = mapsView
        
        if isOnJob {
            
            let jobLocIconView = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
            let jobLocImage = UIImageView(frame: CGRect(x: jobLocIconView.center.x - 10, y:  jobLocIconView.center.y - 5, width: 30,height: 40))
            jobLocImage.image = #imageLiteral(resourceName: "ic_locaton")
            jobLocIconView.addSubview(jobLocImage)
            jobPositionMarker.iconView = jobLocIconView
            jobPositionMarker.map = mapsView
            
            setupPulsator(pulsator: jobPulsator, marker: jobPositionMarker, color: #colorLiteral(red: 0.5647058824, green: 0.9333333333, blue: 0.5647058824, alpha: 1))
            setupPulsator(pulsator: pulsator, marker: marker, color: .clear)
            
        } else {
            setupPulsator(pulsator: pulsator, marker: marker, color: #colorLiteral(red: 0.5647058824, green: 0.9333333333, blue: 0.5647058824, alpha: 1))
        }
    }
    
    private func setupPulsator(pulsator: Pulsator, marker: GMSMarker, color: UIColor) {
        pulsator.backgroundColor = color.cgColor
        pulsator.numPulse = 3
        pulsator.radius = 240.0
        marker.iconView?.transform = CGAffineTransform.init(translationX: 0, y: 200)
        marker.iconView?.cornerRadius = 100
        marker.iconView?.clipsToBounds = true
        marker.iconView?.layer.addSublayer(pulsator)
        pulsator.position = CGPoint(x: 100, y: 100)
    }
}


//MARK: - CLLocationManagerDelegate
extension MapsViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        guard status == .authorizedAlways || status == .authorizedWhenInUse else {
            return
        }
        locationManager.startUpdatingLocation()
        mapsView.isMyLocationEnabled = true
        mapsView.settings.myLocationButton = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard var location = locations.first else {
            return
        }
        
        if let myLocation = self.myLocation {
            location = myLocation
        }
        
        //locationManager.stopUpdatingLocation()
        currentLocation = location.coordinate
        findingServiceProviderView.currentLocation = currentLocation
        findingServiceProviderView.updateLocation()
        
        if doOnce {
            mapsView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            doOnce = false
        }
        marker.position = location.coordinate
        
        if isOnJob {
            let destination = CLLocationCoordinate2D(latitude: Double(currentJob.lat) ?? 0.0, longitude: Double(currentJob.long) ?? 0.0)
            jobPositionMarker.position = destination
            jobPulsator.start()
        }
        pulsator.start()
    }
}


//MARK: - UITableViewDelegate & UITableViewDataSource
extension MapsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isShowingCompleteOrders {
            return completedOrdersList.count
        }
        return proceedingOrdersList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(JobsTableViewCell.self, indexPath: indexPath)
        cell.delegate = self
        cell.selectionStyle = .none
        
        if isShowingCompleteOrders {
            cell.configCell(order: completedOrdersList[indexPath.row], itemIndex: indexPath.row, isOnJob: isOnJob, isCompleteJob: true)
            
        } else {
            cell.configCell(order: proceedingOrdersList[indexPath.row], itemIndex: indexPath.row, isOnJob: isOnJob, isCompleteJob: false)
        }
        
        return cell
    }
}


//MARK: - Keyboard Observers
extension MapsViewController {
    
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(notification:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
        
        if keyboardSize.height > 200  && sendMessageViewBottomConstraint.constant < 100 {
            self.findingServiceProviderView.mileView.isHidden = true
            self.findingServiceProviderView.arrivedView.isHidden = true
            self.findingServiceProviderView.tableView.isHidden = false
            UIView.animate(withDuration: 0.1) {
                self.sendMessageViewBottomConstraint.constant = keyboardSize.height
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
        
        if keyboardSize.height > 200  && sendMessageViewBottomConstraint.constant > 100 {
            self.findingServiceProviderView.mileView.isHidden = false
            self.findingServiceProviderView.arrivedView.isHidden = false
            self.findingServiceProviderView.tableView.isHidden = false
            UIView.animate(withDuration: 0.1) {
                self.sendMessageViewBottomConstraint.constant = 0
                self.view.layoutIfNeeded()
            }
        }
    }
}


//MARK: - JobpopUpDelegate
extension MapsViewController: JobPopupDelegate {
    
    func acceptJob (jobID: Int, customerID: Int) {
        
        PostApi.acceptJob(orderId: jobID, customerID: customerID) { [weak self] (error) in
            
            if error == nil {
                
                if !(self?.proceedingOrdersList.isEmpty ?? false) {
                    self?.getCompletedOrders()
                    self?.getProceedingOrders()
                    return
                    
                } else {
                    self?.startJob(jobID: jobID)
                }
                
            } else if error?.localizedDescription == "Job already taken" {
                self?.showOkAlertWithOKCompletionHandler("Job Already Taken") { ( _) in
                    self?.getCompletedOrders()
                    self?.getProceedingOrders()
                }
            }
        }
    }
    
    func rejectJob() {
        pendingLabel.textColor = #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1)
        completedLabel.textColor = #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1)
        cardContainerView.isHidden = true
        findingServiceProviderView.isHidden = true
        hideShowViews(false)
        self.topNavigationHeightConstraint.constant = 120
        currentJob = Mapper<Job>().map(JSON: [:])!
        isOnJob = false
        
        UIView.animate(withDuration: 0.1) {
            self.view.layoutIfNeeded()
        }
        self.mapsView.clear()
        setupCurrentLocationMark()
        getCompletedOrders()
        getProceedingOrders()
    }
}


//MARK: - DirectionViewDelegate
extension MapsViewController: DirectionViewDelegate {
    
    func arrived() {
        
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.enable = true
        NotificationCenter.default.removeObserver(self)
        
        if currentJob.status == 1 { // NOT YET ARRIVED
            Utility.showLoading()
            
            PostApi.driverArrived(orderId: currentJob.id, customerId: currentJob.customerId) { (error) in
                Utility.hideLoading()
                
                if error == nil {
                    self.currentJob.status = 5
                    self.findingServiceProviderView.updateUI()
                }
            }
            
        } else if currentJob.status == 5 { // HAS ARRIVED
            Utility.showLoading()
            
            PostApi.jobFinished(orderId: currentJob.id, customerId: currentJob.customerId) { (error) in
                Utility.hideLoading()
                
                if error == nil {
                    self.pendingLabel.textColor = #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1)
                    self.completedLabel.textColor = #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1)
                    self.cardContainerView.isHidden = true
                    self.findingServiceProviderView.isHidden = true
                    self.isOnJob = false
                    self.hideShowViews(false)
                    self.topNavigationHeightConstraint.constant = 120
                    self.findingServiceProviderView.currentLocation = CLLocationCoordinate2D()
                    
                    UIView.animate(withDuration: 0.1) {
                        self.view.layoutIfNeeded()
                    }
                    
                    let controller = OrderSuccessViewController()
                    controller.modalPresentationStyle = .overCurrentContext
                    controller.delegate = self
                    self.present(controller, animated: true, completion: nil)
                    self.mapsView.clear()
                    self.setupCurrentLocationMark()
                    
                    self.getCompletedOrders()
                    self.getProceedingOrders()
                    self.setupCurrentLocationMark()
                }
            }
        }
    }
    
    func cancel() {
        Utility.showLoading()
        
        DispatchQueue.main.async {
            let location = "\(self.currentJob.lat),\(self.currentJob.long)"
            PostApi.cancelJob(orderId: self.currentJob.id, location: location) { (error) in
                Utility.hideLoading()
                
                if error == nil {
                    self.pendingLabel.textColor = #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1)
                    self.completedLabel.textColor = #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1)
                    self.cardContainerView.isHidden = true
                    self.findingServiceProviderView.isHidden = true
                    self.isOnJob = false
                    self.hideShowViews(false)
                    self.topNavigationHeightConstraint.constant = 120
                    self.findingServiceProviderView.data = Mapper<Job>().map(JSON: [:])!
                    self.findingServiceProviderView.currentLocation = CLLocationCoordinate2D()
                    
                    UIView.animate(withDuration: 0.1) {
                        self.view.layoutIfNeeded()
                    }
                    self.mapsView.clear()
                    self.setupCurrentLocationMark()
                    self.getCompletedOrders()
                    self.getProceedingOrders()
                }
            }
        }
    }
    
    func sendTapped(message: String) {
        
        User.shared.socket.emit("chat", [
            "id": currentJob.customerId,
            "order_id": currentJob.id,
            "message": message,
            "send_by": 2
        ])
    }
}


//MARK: - OrderSuccessViewControllerDelegate
extension MapsViewController: OrderSuccessViewControllerDelegate {
    
    func continueSuccess() {
        pendingLabel.textColor = #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1)
        completedLabel.textColor = #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1)
        let controller = SetRatingViewController()
        controller.job = currentJob
        controller.modalPresentationStyle = .overCurrentContext
        self.present(controller, animated: true, completion: nil)
    }
}


//MARK: - JobsTableViewCellDelegate
extension MapsViewController: JobsTableViewCellDelegate {
    
    func proceedButtonTapped(itemIndex: Int) {
        
        let order = proceedingOrdersList[itemIndex]
        
        if let job = Mapper<Job>().map(JSON: [
            "customer_image": order.customerImage,
            "customer_username": order.customerName,
            "id": order.id,
            "customer_id": order.customerId,
            "order_address": order.customerAddress,
            "order_images": order.orderImages,
            "order_location": order.customerLocation,
            "user_options": "",
            "order_detail": order.description,
            "stars": order.stars,
            "status": order.status
        ]) {
            
            if order.status == -1 {
                currentJob = job
                startJob(jobID: job.id)
                
            } else {
                currentJob = job
                IQKeyboardManager.shared.enableAutoToolbar = false
                IQKeyboardManager.shared.enable = false
                self.registerKeyboardNotifications()
                self.cardContainerView.isHidden = false
                self.findingServiceProviderView.isHidden = false
                self.hideShowViews(true)
                self.isOnJob = true
                tableViewContainer.isHidden = true
                cardContainerView.isHidden = false
                self.tableView.isHidden = true
                
                if self.currentJob.status == -1 {
                    self.currentJob.status = 1
                }
                
                self.findingServiceProviderView.data = currentJob
                self.findingServiceProviderView.currentLocation = currentLocation
                
                self.findingServiceProviderView.loadData()
                self.findingServiceProviderView.populateData()
                self.findingServiceProviderView.updateLocation()
                self.topNavigationHeightConstraint.constant = 80
                
                UIView.animate(withDuration: 0.1) {
                    self.view.layoutIfNeeded()
                }
                
                self.getCompletedOrders()
                self.getProceedingOrders()
                setupCurrentLocationMark()
            }
        }
    }
}


//MARK: -
extension MapsViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        // Custom logic here
        let marker = GMSMarker()
        marker.position = coordinate
        myLocation = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        marker.title = "I added this with a long tap"
        marker.snippet = ""
        marker.map = mapView
    }
}
