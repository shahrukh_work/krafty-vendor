//
//  JobsTableViewCell.swift
//  Krafty-Vendor
//
//  Created by Mac on 29/04/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class JobsTableViewCell: UITableViewCell {

    
    //MARK: - Outlet
    @IBOutlet weak var customerNameLabel: UILabel!
    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var customerImageView: UIImageView!
    @IBOutlet weak var proceedButton: UIButton!
    

    //MARK: - Variable
    var delegate: JobsTableViewCellDelegate?
    var itemIndex = -1


    //MARK: - View Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - Actions
    @IBAction func proceedButtonTapped(_ sender: Any) {
        delegate?.proceedButtonTapped(itemIndex: itemIndex)
    }
    
    
    //MARK: - Methods
    func configCell(order: Order, itemIndex: Int, isOnJob: Bool, isCompleteJob: Bool) {
        self.itemIndex = itemIndex
        customerNameLabel.text = order.customerName
        orderIdLabel.text = "Job \(order.id)"
        descriptionLabel.text = order.description
        addressLabel.text = order.customerAddress
        let time = Utility.convertDateFormatter(date: order.startDateTime, inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "h:mm")
        let date = Utility.convertDateFormatter(date: order.startDateTime, inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "MMM dd - yyyy")
        dateTimeLabel.text = "Time \(time)    \(date)"
        
        if let url = URL(string: APIRoutes.customerProfileImageUrl + order.customerImage) {
            customerImageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "ic_image"))
        }
        
        if isOnJob {
            proceedButton.alpha = 0.5
            proceedButton.isUserInteractionEnabled = !isOnJob
            
        }
        
        if order.status == -1 {
            proceedButton.setTitle("Pending", for: .normal)
            proceedButton.alpha = 0.5
            proceedButton.isUserInteractionEnabled = false
            
        } else {
            proceedButton.setTitle("Proceed", for: .normal)
            proceedButton.alpha = 1
            proceedButton.isUserInteractionEnabled = true
        }
        
        if isCompleteJob {
            proceedButton.alpha = 0.5
            proceedButton.isUserInteractionEnabled = false
            
        }
    }
}
