//
//  IncomingMessageTableViewCell.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/29/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class IncomingMessageMapTableViewCell: UITableViewCell {
    
    
    //MARK: - Outlets
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var senderImageView: UIImageView!
    @IBOutlet weak var messageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var timeLabel: UILabel!
    
    
    //MARK: - View  Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - Methods
    func configCell(message: String) {
        showIncomingMessage(text: message)
    }
    
    func showIncomingMessage(text: String) {
        let color = #colorLiteral(red: 0.9176470588, green: 0.9098039216, blue: 0.9254901961, alpha: 1)
        let label =  UILabel()
        label.numberOfLines = 0
        label.font = UIFont.font(withSize: 14)
        label.textColor = #colorLiteral(red: 0.137254902, green: 0.1215686275, blue: 0.1254901961, alpha: 1)
        label.text = text
        
        messageView.subviews.forEach { (view) in
            if view.isKind(of: UIImageView.self) || view.isKind(of: UILabel.self) {
                view.removeFromSuperview()
            }
        }
        let constraintRect = CGSize(width: 0.66 * messageView.frame.width,
                                    height: .greatestFiniteMagnitude)
        let boundingBox = text.boundingRect(with: constraintRect,
                                            options: .usesLineFragmentOrigin,
                                            attributes: [.font: label.font as Any],
                                            context: nil)
        label.frame.size = CGSize(width: ceil(boundingBox.width),
                                  height: ceil(boundingBox.height))
        
        let bubbleImageSize = CGSize(width: label.frame.width + 28,
                                     height: label.frame.height + 20)
        
        let incomingMessageView = UIImageView(frame:
            CGRect(x: 0,
                   y: 0,
                   width: bubbleImageSize.width,
                   height: bubbleImageSize.height))
        
        let bubbleImage = UIImage(named: "incoming-message-bubble")!
            .resizableImage(withCapInsets: UIEdgeInsets(top: 17, left: 21, bottom: 17, right: 21),
                            resizingMode: .stretch)
            .withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        
        incomingMessageView.image = bubbleImage
        incomingMessageView.tintColor = color
        
        messageView.addSubview(incomingMessageView)
        
        label.center = incomingMessageView.center
        messageView.addSubview(label)
        messageViewHeightConstraint.constant = incomingMessageView.frame.height
    }
}
