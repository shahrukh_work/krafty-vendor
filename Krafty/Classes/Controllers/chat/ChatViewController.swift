//
//  ChatViewController.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/28/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class ChatViewController: UIViewController {
    
    
    //MARK: - Outlets
    @IBOutlet weak var chatTextBoxBottomContraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var sendMessageViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var messageTextViewHeightConstraint: NSLayoutConstraint!

    
    //MARK: - Variables
    var messages = [String]()
    var isKeyboardShowingFirstTime = true

    
    //MARK: - ViewController  Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupView()
        registerKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.enable = true
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - View Setup
    private func setupView() {
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        tableView.delegate = self
        tableView.dataSource = self
        messageTextView.delegate = self
        reloadViews()
    }
    
    
    //MARK: - Actions
    @IBAction func sendButonPressed(_ sender: Any) {
        messageTextView.resignFirstResponder()
        
        if messageTextView.text != "" {
            messages.append(messageTextView.text)
            reloadViews()
        }
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - Private Methods
    private func reloadViews() {
        tableView.reloadData()
        tableView.layoutIfNeeded()
        let scrollPoint = CGPoint(x: 0, y: self.tableView.contentSize.height - self.tableView.frame.size.height)
        self.tableView.setContentOffset(scrollPoint, animated: false)
        messageTextViewHeightConstraint.constant = 40
        messageTextView.text = "Type"
        messageTextView.textColor = #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1)
    }
}


//MARK: - TableView Delegate & DataSource
extension ChatViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 52
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.register(DateSectionHeaderTableViewCell.self, indexPath: IndexPath(row: 0, section: section ))
        
        if messages.count == 0 {
            header.configCell(date: "")
        
        } else {
            header.configCell(date: "Today")
        }
        return header
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row % 2 == 0 {
            let cell = tableView.register(IncomingMessageTableViewCell.self, indexPath: indexPath)
            cell.configCell(message: messages[indexPath.row])
            return cell
        
        } else {
            let cell = tableView.register(OutgoingMessageTableViewCell.self, indexPath: indexPath)
            cell.configCell(message: messages[indexPath.row])
            return cell
        }
    }
}


//MARK: - TextView Delegate
extension ChatViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.textColor == #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1) {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == "" {
            messageTextViewHeightConstraint.constant = 40
            textView.text = "Type"
            textView.textColor = #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1)
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let minTextViewHeight: CGFloat = 40
        let maxTextViewHeight: CGFloat = 120

        var height = ceil(textView.contentSize.height) // ceil to avoid decimal

        if (height < minTextViewHeight) {
            height = minTextViewHeight
        }
        
        if (height > maxTextViewHeight) {
            height = maxTextViewHeight
        }

        if height != messageTextViewHeightConstraint.constant {
            messageTextViewHeightConstraint.constant = height
            textView.setContentOffset(.zero, animated: false) // scroll to top to avoid "wrong contentOffset" artefact when line count changes
        }
    }
}


extension ChatViewController {
    
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(notification:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
                
        if keyboardSize.height > 200  && chatTextBoxBottomContraint.constant < 100{
            let window = UIApplication.shared.keyWindow
            
            UIView.animate(withDuration: 0.1) {
                
                if let win = window {
                    self.chatTextBoxBottomContraint.constant = self.chatTextBoxBottomContraint.constant + keyboardSize.height -  win.safeAreaInsets.bottom
                }
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
        
        if keyboardSize.height > 200  && chatTextBoxBottomContraint.constant > 100 {
            
            UIView.animate(withDuration: 0.1) {
                self.chatTextBoxBottomContraint.constant = 8
                self.view.layoutIfNeeded()
            }
        }
    }
}
