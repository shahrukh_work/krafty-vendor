//
//  OutgoingMessageTableViewCell.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/29/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class OutgoingMessageTableViewCell: UITableViewCell {

    
    //MARK: - Outlets
    @IBOutlet weak var receiverImageView: UIImageView!
    @IBOutlet weak var messageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var tickImageView: UIImageView!
    
    
    //MARK: - Variables
    var messageTextAddedToMessageView = false
    
    
    //MARK: - View  Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - Methods
    func configCell(message: String) {
        showOutgoingMessage(text: message)
    }
    
    func showOutgoingMessage(text: String) {
        let color = #colorLiteral(red: 0.564625144, green: 0.9315071702, blue: 0.5658783317, alpha: 1)
        let label =  UILabel()
        label.numberOfLines = 0
        label.font = UIFont.font(withSize: 14)
        label.textColor = .white
        label.text = text
        
        messageView.subviews.forEach { (view) in
            if view.isKind(of: UIImageView.self) || view.isKind(of: UILabel.self) {
                view.removeFromSuperview()
            }
        }
        let constraintRect = CGSize(width: 0.66 * messageView.frame.width,
                                    height: .greatestFiniteMagnitude)
        let boundingBox = text.boundingRect(with: constraintRect,
                                            options: .usesLineFragmentOrigin,
                                            attributes: [.font: label.font as Any],
                                            context: nil)
        label.frame.size = CGSize(width: ceil(boundingBox.width),
                                  height: ceil(boundingBox.height))
        
        let bubbleImageSize = CGSize(width: label.frame.width + 28,
                                     height: label.frame.height + 20)
        
        let outgoingMessageView = UIImageView(frame:
            CGRect(x: messageView.frame.width - bubbleImageSize.width,
                   y: 0,
                   width: bubbleImageSize.width,
                   height: bubbleImageSize.height))
        
        let bubbleImage = UIImage(named: "outgoing-message-bubble")?
            .resizableImage(withCapInsets: UIEdgeInsets(top: 17, left: 21, bottom: 17, right: 21),
                            resizingMode: .stretch)
            .withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        
        outgoingMessageView.image = bubbleImage
        outgoingMessageView.tintColor = color
        messageView.addSubview(outgoingMessageView)
        label.center = outgoingMessageView.center
        messageView.addSubview(label)
        messageViewHeightConstraint.constant = outgoingMessageView.frame.height
    }
}
