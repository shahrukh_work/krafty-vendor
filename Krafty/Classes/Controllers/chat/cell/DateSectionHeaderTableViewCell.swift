//
//  DateSectionHeaderTableViewCell.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/29/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class DateSectionHeaderTableViewCell: UITableViewCell {

    
    //MARK: - Outlets
    @IBOutlet weak var dateLabel: UILabel!
    
    
    //MARK: - View  Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - Methods
    func configCell(date: String) {
        dateLabel.text = date
    }
}
