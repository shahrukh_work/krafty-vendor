//
//  NotificationsTableViewCell.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/27/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class NotificationsTableViewCell: UITableViewCell {

    
    //MARK: - Outlets
    @IBOutlet weak var subjectDateLabel: UILabel!
    @IBOutlet weak var detailTimeLabel: UILabel!
    @IBOutlet weak var notificationDetailLabel: UILabel!
    @IBOutlet weak var notificationSubjectLabel: UILabel!
    @IBOutlet weak var circleImageView: UIImageView!
    @IBOutlet weak var locationImageView: UIImageView!
    
    
    //MARK: - Variables
    var isCellForLocation = true
    
    
    //MARK: - View  Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - Methods
    func configCell(notification: Notification) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let date = dateFormatter.date(from: notification.createdAt) ?? Date()
        
        let timeFormatter = DateFormatter()
        timeFormatter.locale = Locale(identifier: "en_US_POSIX")
        timeFormatter.dateFormat = "MMM d"
        timeFormatter.timeZone = TimeZone.current
        
        let dateHours = Calendar.current.dateComponents([.hour], from: Date(), to: date)
        
        subjectDateLabel.text = timeFormatter.string(from: date)
        notificationSubjectLabel.text = notification.message
        notificationDetailLabel.text = notification.title
        detailTimeLabel.text = "\(dateHours.hour ?? 0)hr ago"
    }
}
