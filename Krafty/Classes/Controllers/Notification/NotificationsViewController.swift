//
//  NotificationsViewController.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/27/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class NotificationsViewController: UIViewController {

    
    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var noNotificationView: UIView!
    
    
    //MARK: - Variables
    var notifications = [Notification]()
    
    
    //MARK: - ViewController  Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
    //MARK: - View Setup
    private func setupView() {
        tableView.delegate = self
        tableView.dataSource = self
        loadData()
    }
    
    
    //MARK: - Actions
    @IBAction func clearButtonTapped(_ sender: Any) {
        Utility.showLoading()
        
        PostApi.clearNotifiaction { (error) in
            Utility.hideLoading()
            
            if error == nil {
                self.notifications.removeAll()
                self.tableView.reloadData()
            }
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - Private Methods
    private func loadData() {
        
        Notifications.getNotifications { (data, error, message) in
            
            if error == nil {
                
                if let data = data {
                    self.notifications = data.data
                    self.tableView.reloadData()
                }
            }
        }
    }
}


//MARK: - TableView Delegate & DataSource
extension NotificationsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        noNotificationView.isHidden = !notifications.isEmpty
        return notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(NotificationsTableViewCell.self, indexPath: indexPath)
        cell.configCell(notification: notifications[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Utility.showLoading()
        
        PostApi.readNotifiaction(notificationID: notifications[indexPath.row].id) { (error) in
            Utility.hideLoading()
        }
    }
}
