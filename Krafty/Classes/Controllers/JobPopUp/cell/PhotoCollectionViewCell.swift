//
//  PhotoCollectionViewCell.swift
//  Krafty-Vendor
//
//  Created by Mac on 29/04/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import SDWebImage


class PhotoCollectionViewCell: UICollectionViewCell {
    
    
    //MARK: - Outlets
    @IBOutlet weak var itemImageView: UIImageView!
    
    
    //MARK: - View Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - Public Methods
    func config(url: String) {
        
        if let url = URL(string: APIRoutes.orderImages + url) {
            itemImageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "ic_login_logo"))
        }
    }
}
