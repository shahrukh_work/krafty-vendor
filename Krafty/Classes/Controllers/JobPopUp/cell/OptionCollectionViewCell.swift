//
//  OptionCollectionViewCell.swift
//  Krafty-Vendor
//
//  Created by Mac on 29/04/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class OptionCollectionViewCell: UICollectionViewCell {

    
    //MARK: - Outlet
    @IBOutlet weak var optionLabel: PaddingLabel!
    
    
    //MARK: - View Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
