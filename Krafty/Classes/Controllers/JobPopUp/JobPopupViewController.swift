//
//  JobPopupViewController.swift
//  Krafty-Vendor
//
//  Created by Mac on 29/04/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import ObjectMapper
import GoogleMaps
import Alamofire
import SwiftyJSON


class JobPopupViewController: UIViewController {

    
    //MARK: - Outlet
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var personImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var jobNumLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UITextView!
    @IBOutlet weak var jobPhotoCollectionView: UICollectionView!
    @IBOutlet weak var optionCollectionView: UICollectionView!
    
    
    //MARK: - Variable
    weak var delegate: JobPopupDelegate?
    var data: Job = Mapper<Job>().map(JSON: [:])!
    var currentLocation = CLLocationCoordinate2D()

    
    //MARK: - Controller Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup ()
    }
    
    
    //MARK: - Setup View
    func setup () {
        descriptionLabel.isEditable = false
        jobPhotoCollectionView.delegate = self
        jobPhotoCollectionView.dataSource = self
        
        optionCollectionView.delegate = self
        optionCollectionView.dataSource = self
        
        populateData()
    }
    
    
    //MARK: - Actions
    @IBAction func rejectPressed(_ sender: Any) {
        delegate?.rejectJob()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func acceptPressed(_ sender: Any) {
        delegate?.acceptJob(jobID: data.id, customerID: data.customerId)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: - Private Methods
    func populateData() {
        let dest = CLLocationCoordinate2D(latitude: Double(data.lat) ?? 0.0, longitude: Double(data.long) ?? 0.0)
        calculateTimeDistance(src: currentLocation, dst: dest)
        
        if let url = URL(string: APIRoutes.customerProfileImageUrl + data.customerImage) {
            personImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "ic_image"))
        }
        descriptionLabel.text = data.orderDetails
        nameLabel.text = data.customerName
        jobNumLabel.text = "Job #\(data.id)"
        locationLabel.text = data.orderAddress
    }
    
    private func calculateTimeDistance(src: CLLocationCoordinate2D, dst: CLLocationCoordinate2D){
        let origin = "\(src.latitude),\(src.longitude)"
        let destination = "\(dst.latitude),\(dst.longitude)"
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(kGoogleMapsAPI)"
        Utility.showLoading()
        
        Alamofire.request(url).responseJSON { [weak self] response in
            
            do {
                let json = try JSON(data: response.data!)
                let route = json["routes"].arrayValue.first
                
                if let route = route {
                    let leg = route["legs"].arrayValue.first
                    
                    if let leg = leg {
                        let distance = leg["distance"].dictionaryObject?["text"]
                        let time = leg["duration"].dictionaryObject?["text"]
                        
                        if let distance = distance as? String, let time = time as? String {
                            self?.distanceLabel.text = distance
                            self?.timeLabel.text = time
                        }
                    }
                }
                
                Utility.hideLoading()
                
            } catch {
                Utility.hideLoading()
                print(error.localizedDescription)
            }
        }
    }
}

//MARK: - UICollectionViewDelegate & UICollectionViewDataSource
extension JobPopupViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == jobPhotoCollectionView {
            return data.orderImages.count
        }
        return data.userOptions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == jobPhotoCollectionView {
            let cell = collectionView.register(PhotoCollectionViewCell.self, indexPath: indexPath)
            cell.config(url: data.orderImages[indexPath.row])
            return cell
            
        } else {
            let cell = collectionView.register(OptionCollectionViewCell.self, indexPath: indexPath)
            cell.optionLabel.text = data.userOptions[indexPath.row]
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == optionCollectionView {
            let label = UILabel(frame: CGRect.zero)
            label.text = data.userOptions[indexPath.row]
            label.sizeToFit()
            return CGSize(width: label.frame.width + 10, height: 30)
        }
        return CGSize(width: 55, height: 40)
    }
}


