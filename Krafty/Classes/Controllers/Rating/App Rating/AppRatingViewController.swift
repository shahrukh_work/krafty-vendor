//
//  AppRatingViewController.swift
//  Krafty-Vendor
//
//  Created by Mac on 30/04/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import Cosmos

class AppRatingViewController: UIViewController, UITextViewDelegate {
    
    //MARK: - Outlets
    @IBOutlet weak var ratingCategoryLabel: UILabel!
    @IBOutlet weak var ratingNumberLabel: UILabel!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var cosmoView: CosmosView!
    
    
    //MARK: - Variable
    
    
    //MARK: - Controller Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup ()
    }
    
    
    //MARK: - Setup View
    func setup () {
        messageTextView.text = "Write Something  ..."
        messageTextView.textColor = UIColor.lightGray
        messageTextView.delegate =  self
        
        cosmoView.didFinishTouchingCosmos = { rating in
            self.ratingNumberLabel.text = "You rated \(Int(rating)) star"
        }
    }
    
    //MARK: - Actions
    @IBAction func submitPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
}
