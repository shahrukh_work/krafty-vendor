//
//  SetRatingViewController.swift
//  Krafty-Vendor
//
//  Created by Mac on 30/04/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import Cosmos
import ObjectMapper


class SetRatingViewController: UIViewController, UITextViewDelegate {

    
    //MARK: - Variable
    var job = Mapper<Job>().map(JSON: [:])!
    
    
    //MARK: - Outlet
    @IBOutlet weak var personImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingCategoryLabel: UILabel!
    @IBOutlet weak var ratingNumberLabel: UILabel!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var cosmoView: CosmosView!


    //MARK: - Controller Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup ()
    }
    
    
    //MARK: - Setup View
    func setup () {
        messageTextView.text = "Write Something  ..."
        messageTextView.textColor = UIColor.lightGray
        messageTextView.delegate =  self
        
        cosmoView.didFinishTouchingCosmos = { rating in
            self.ratingNumberLabel.text = "You rated \(Int(rating)) star"
        }
        
        populateData()
    }
    
    
    //MARK: - Actions
    @IBAction func submitPressed(_ sender: Any) {
        
        Utility.showLoading()
        PostApi.addReview(customerId: job.customerId, orderId: job.id, performance: "", stars: Int(cosmoView.rating), message: messageTextView.text ?? "") { (error) in
            Utility.hideLoading()
            
            if error == nil {
                self.showOkAlertWithOKCompletionHandler("Rating Added...") { ( _) in
                    self.dismiss(animated: true, completion: nil)
                }
                
            } else {
                self.showOkAlertWithOKCompletionHandler("Something went wrong...") { ( _) in
                    self.dismiss(animated: true, completion: nil)
                }
            }
            
        }
    }
    
    
    //MARK: - UITextViewDelegate
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    
    //MARK: - Private Methods
    func populateData() {
        
        if let url = URL(string: APIRoutes.customerProfileImageUrl + job.customerImage) {
            personImageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "ic_image"))
        }
        nameLabel.text = job.customerName
    }
    
}
