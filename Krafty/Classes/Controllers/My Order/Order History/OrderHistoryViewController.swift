//
//  OrderHistoryViewController.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 5/6/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import ObjectMapper

class OrderHistoryViewController: UIViewController {

    
    //MARK: - Variables
    var isCurrentOrder = false
    var orderDetails = Mapper<OrderDetails>().map(JSON: [:])
    let images = [ #imageLiteral(resourceName: "service1") , #imageLiteral(resourceName: "service2") ]
    var data = [ServiceTypeData]()
    var delegate: RegisterComplaintDelegate?
    
    //MARK: -
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var contractorTypeLabel: UILabel!
    @IBOutlet weak var jobNumLabel: UILabel!
    @IBOutlet weak var jobTypeLabel: UILabel!
    @IBOutlet weak var jobTimeLabel: UILabel!
    @IBOutlet weak var jobDateLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    @IBOutlet weak var optionsCollectionView: UICollectionView!
    @IBOutlet weak var contractorImageView: UIImageView!
    @IBOutlet weak var contractorNameLabel: UILabel!
    @IBOutlet weak var contractorRatingLabel: UILabel!
    
    
    //MARK: - ViewController  Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
    //MARK: - View Setup
    private func setupView() {
        loadData()
        contentView.cornerRadius = 9
        contentView.addShadow(offset: CGSize(width: 0, height: 0), color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.26), opacity: 1.0, radius: 0.5)
        contentView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        imagesCollectionView.delegate = self
        imagesCollectionView.dataSource = self
        optionsCollectionView.delegate = self
        optionsCollectionView.dataSource = self
    }
    
    
    //MARK: - Private Method
    private func loadData() {
        contractorTypeLabel.text = orderDetails?.details?.serviceName
        jobNumLabel.text = "Job # \(orderDetails?.details?.id ?? 0)"
        jobDateLabel.text = "\(Utility.convertDateFormatter(date: orderDetails?.details?.startDateTime ?? "", inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "dd MMMM"))"
        locationLabel.text = orderDetails?.details?.customerAddress
        messageLabel.text = orderDetails?.details?.description
        orderDetails?.details?.imageUrls = orderDetails?.details?.images.split(separator: ",").map{String($0)} ?? [""]
        contractorNameLabel.text = orderDetails?.details?.driverName
        contractorImageView.sd_setImage(with: URL(string: "\(APIRoutes.customerImageUrl)" + (orderDetails?.details!.customerImage)!), placeholderImage: UIImage(named: "male.png"))
        contractorRatingLabel.text = "\(orderDetails!.details!.stars)"
        
        if isCurrentOrder {
            jobTimeLabel.text = "\(Utility.convertDateFormatter(date: orderDetails?.details?.startDateTime ?? "", inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "h a"))"
        
        } else {
            jobTimeLabel.text = "\(Utility.convertDateFormatter(date: orderDetails?.details?.startDateTime ?? "", inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "h a")) - \(Utility.convertDateFormatter(date: orderDetails?.details?.endDateTime ?? "", inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "h a"))"
        }
        
        data.append(ServiceTypeData(title: "Option 1", isSelected: true))
        data.append(ServiceTypeData(title: "Option 2", isSelected: true))
        optionsCollectionView.reloadData()
        imagesCollectionView.reloadData()
        // TODO: -  after meeting Taimur has said to leave subServices
    }
    
    
    //MARK: - Actions
    @IBAction func okButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func registerButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        delegate?.registerComplaint(orderId: (orderDetails?.details!.id)!, driverId: (orderDetails?.details!.driverId)!, contractorName: (orderDetails?.details!.driverName)!)
    }
}


//MARK: - UICollection Delegate & DataSource
extension OrderHistoryViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == optionsCollectionView {
            return data.count
        }
        return orderDetails?.details?.imageUrls.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == optionsCollectionView {
            let cell = collectionView.register(ServiceTypeCollectionViewCell.self, indexPath: indexPath)
            cell.configure(data: data[indexPath.row])
            return cell
        }
        
        let cell = collectionView.register(ImageCollectionViewCell.self, indexPath: indexPath)
        cell.config(imageName: orderDetails?.details?.imageUrls[indexPath.row] ?? "")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == optionsCollectionView {
            let label = UILabel(frame: CGRect.zero)
            label.font = UIFont(name: "Poppins-Regular", size: 14)
            label.text = data[indexPath.row].title
            label.sizeToFit()
            return CGSize(width: label.frame.width + 10, height: 23)
        }
        return CGSize(width: 56, height: 45)
    }
}
