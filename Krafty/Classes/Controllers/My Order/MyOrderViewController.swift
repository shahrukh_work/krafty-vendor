//
//  MyOrderViewController.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/27/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class MyOrderViewController: UIViewController {
    
    
    //MARK: - Variables
    var showCurrentOrders = false
    var allOrders = [Order]()
    
    
    //MARK: - Outlets
    @IBOutlet weak var curvedView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noOrdersView: UIView!
    
    
    //MARK: - ViewController  Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
    //MARK: - View Setup
    private func setupView() {
        tableView.delegate = self
        tableView.dataSource = self
        getAllOrders()
    }
    
    
    //MARK: - Private Methods
    
    private func getAllOrders() {
        
        Orders.getAllOrders() { (data, error, message) in
            
            if error == nil {
                self.allOrders = data!.allOrders
                self.tableView.reloadData()
                
            } else {
                self.showOkAlert("There was an error. Please try again.")
            }
        }
    }
    
    
    //MARK: - Actions
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}


//MARK: - TableView Delegate & DataSource
extension MyOrderViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        noOrdersView.isHidden = !allOrders.isEmpty
        return allOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(OrderTableViewCell.self, indexPath: indexPath)
        cell.selectionStyle = .none
        cell.configCell(isCurrentOrder: showCurrentOrders, order: allOrders[indexPath.row])
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let orderId = allOrders[indexPath.row].id
        
        OrderDetails.getOrderDetails(orderId: orderId) { (data, error, message) in
            
            if error == nil {
                let controller = OrderHistoryViewController()
                controller.delegate = self
                controller.isCurrentOrder = self.showCurrentOrders
                controller.orderDetails = data
                controller.modalPresentationStyle = .overCurrentContext
                self.present(controller, animated: true)
                
            } else {
                self.showOkAlert(kErrorMessage)
            }
        }
    }
}


//MARK: - RegisterComplaintDelegate
extension MyOrderViewController: RegisterComplaintDelegate {
    func registerComplaint(orderId: Int, driverId: Int, contractorName: String) {
        let controller = ComplaintViewController()
        controller.orderId = orderId
        controller.customerId = driverId
        controller.customerName = contractorName
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
