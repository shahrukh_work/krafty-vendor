//
//  ServiceTypeCollectionViewCell.swift
//  Krafty
//
//  Created by Bilal Saeed on 4/28/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class ServiceTypeCollectionViewCell: UICollectionViewCell {

    //MARK: - Outlets
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    
    //MARK: - View Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(data: ServiceTypeData) {
        contentLabel.text = data.title
        
        if data.isSelected {
            contentLabel.textColor = #colorLiteral(red: 0.5868938565, green: 0.929741919, blue: 0.5990998149, alpha: 1)
            containerView.borderColor = #colorLiteral(red: 0.5868938565, green: 0.929741919, blue: 0.5990998149, alpha: 1)
            
        } else {
            contentLabel.textColor = #colorLiteral(red: 0.6117647059, green: 0.6117647059, blue: 0.6117647059, alpha: 1)
            containerView.borderColor = #colorLiteral(red: 0.6941176471, green: 0.7098039216, blue: 0.7411764706, alpha: 1)
        }
    }
    
    func configure(subService: SubService) {
        contentLabel.text = subService.name
        
        if subService.isSelected {
            contentLabel.textColor = #colorLiteral(red: 0.5868938565, green: 0.929741919, blue: 0.5990998149, alpha: 1)
            containerView.borderColor = #colorLiteral(red: 0.5868938565, green: 0.929741919, blue: 0.5990998149, alpha: 1)
            
        } else {
            contentLabel.textColor = #colorLiteral(red: 0.6117647059, green: 0.6117647059, blue: 0.6117647059, alpha: 1)
            containerView.borderColor = #colorLiteral(red: 0.6941176471, green: 0.7098039216, blue: 0.7411764706, alpha: 1)
        }
    }
}
