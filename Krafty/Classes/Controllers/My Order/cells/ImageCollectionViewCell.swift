//
//  ImageCollectionViewCell.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 5/5/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {

    
    //MARK: Outlets
    @IBOutlet var containerView: UIView!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var imageViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet var imageViewHeightConstraint: NSLayoutConstraint!
    var isDashedLayerAdded = false
    
    
    //MARK: - View  Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    
    //MARK: - Methods
    func config(image: UIImage, isImagePicker: Bool) {
        imageView.image = image
    }
    
    func config(imageName: String) {
        imageView.sd_setImage(with: URL(string: "\(APIRoutes.orderImageUrl)/" + imageName), placeholderImage: UIImage(named: "male "))
    }
    
    
//    //MARK: Private Methods
//    func dashedBorderLayer() -> CAShapeLayer {
//       let  borderLayer = CAShapeLayer()
//        borderLayer.name  = "borderLayer"
//         let frameSize = containerView.frame.size
//        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
//        borderLayer.bounds = shapeRect
//        borderLayer.position = CGPoint( x: frameSize.width/2,y: frameSize.height/2)
//        borderLayer.fillColor = UIColor.clear.cgColor
//        borderLayer.strokeColor = #colorLiteral(red: 0.6588235294, green: 0.6549019608, blue: 0.6941176471, alpha: 1)
//        borderLayer.lineWidth = 2
//        borderLayer.lineJoin = CAShapeLayerLineJoin.round
//        borderLayer.lineDashPattern = NSArray(array: [NSNumber(value:5),NSNumber(value:3)]) as? [NSNumber]
//        let path = UIBezierPath.init(roundedRect: shapeRect, cornerRadius: 5)
//        borderLayer.path = path.cgPath
//        return borderLayer
//    }
}
