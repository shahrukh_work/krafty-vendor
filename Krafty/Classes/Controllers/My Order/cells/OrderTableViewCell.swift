//
//  OrderTableViewCell.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/27/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class OrderTableViewCell: UITableViewCell {
    
    
    //MARK: - Outlets
    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var serviceTypeLabel: UILabel!
    @IBOutlet weak var orderDateLabel: UILabel!
    @IBOutlet weak var orderDurationLabel: UILabel!
    
    
    //MARK: - View  Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - Methods
    func configCell(isCurrentOrder: Bool, order: Order) {
        orderIdLabel.text = "Job # \(order.id)"
        serviceTypeLabel.text = "\(order.serviceName)"
        orderDateLabel.text = "\(Utility.convertDateFormatter(date: order.startDateTime, inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "dd MMMM"))"
        
        if isCurrentOrder {
            orderDurationLabel.text = "\(Utility.convertDateFormatter(date: order.startDateTime, inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "h a"))"
        
        } else {
            orderDurationLabel.text = "\(Utility.convertDateFormatter(date: order.startDateTime, inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "h a")) - \(Utility.convertDateFormatter(date: order.endDateTime, inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "h a"))"
        }
    }
}
