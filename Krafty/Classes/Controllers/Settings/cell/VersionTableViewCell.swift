//
//  VersionTableViewCell.swift
//  Krafty
//
//  Created by Mac on 27/04/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class VersionTableViewCell: UITableViewCell {

    
    //MARK: - Outlet
    @IBOutlet weak var versionLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
