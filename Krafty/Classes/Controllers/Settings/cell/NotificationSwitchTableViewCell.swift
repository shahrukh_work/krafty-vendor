//
//  NotificationSwitchTableViewCell.swift
//  Krafty
//
//  Created by Mac on 27/04/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class NotificationSwitchTableViewCell: UITableViewCell {

    
    //MARK: - Variables
    var switchState: ((_ state: Bool) -> ())?
    
    
    //MARK: - Outlets
    @IBOutlet weak var notificationSwitch: UISwitch!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    //MARK: - Actions
    @IBAction func switchPressed(_ sender: UISwitch) {
        switchState?(sender.isOn)
    }
    
    
    //MARK: - Methods
    func configCell(isNotificationOn: Bool) {
        notificationSwitch.isOn = isNotificationOn
    }
}
