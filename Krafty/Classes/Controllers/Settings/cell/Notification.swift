//
//  Notification.swift
//  Krafty
//
//  Created by Mac on 27/04/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias NotificationsCompletionHandler = (_ data: Notifications?, _ error: NSError?, _ message: String) -> Void

class Notifications: Mappable {
    
    var error = false
    var message = ""
    var data = [Notification]()
    var errors = [String]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        error   <- map["error"]
        message <- map["message"]
        data    <- map["data"]
        errors  <- map["errors"]
    }
    
    class func getNotifications(completion: @escaping NotificationsCompletionHandler) {
        
        Utility.showLoading()
        
        APIClient.shared.getNotifications() { (data, error, message) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = Mapper<Notifications>().map(JSONObject: data) {
                    completion(data, nil, "")
                
                } else {
                    completion(Mapper<Notifications>().map(JSON: [:])!, nil, "")
                }
            
            } else {
                completion(Mapper<Notifications>().map(JSON: [:])!, error, "Error!")
            }
        }
    }
}


class Notification: Mappable {
    
    var id = -1
    var foreignID = -1
    var type = ""
    var title = ""
    var message = ""
    var driverID = -1
    var status = -1
    var createdAt = ""
    var updatedAt = ""
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id          <- map["id"]
        foreignID   <- map["foreign_id"]
        type        <- map["type"]
        title       <- map["title"]
        message     <- map["message"]
        driverID    <- map["driver_id"]
        status      <- map["status"]
        createdAt   <- map["created_at"]
        updatedAt   <- map["updated_at"]
    }
}

