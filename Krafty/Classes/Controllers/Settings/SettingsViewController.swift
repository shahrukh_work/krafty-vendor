//
//  SettingsViewController.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/24/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    
    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    let settingData = ["Edit Profile", "Rating of the app", "Credit/Debit Card Settings","Notifications", "App version"]
    var isNotificationOn = false
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
    //MARK: - Setup
    func setupView () {
        tableView.delegate = self
        tableView.dataSource = self
        
        NotificationStatus.getNotificationStatus() { (data,error,message) in
            
            if error == nil {
                
                if data?.status == 0 {
                    self.isNotificationOn = false

                } else {
                    self.isNotificationOn = true
                }
                self.tableView.reloadData()
            
            } else {
                self.showOkAlert("Error! Couldn't get notification status of the app.")
            }
        }
    }
    
    
    //MARK: - Actions
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}


//MARK: - UITableViewDelegate & UITableViewDataSource
extension SettingsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        settingData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 4 {
            let cell = tableView.register(VersionTableViewCell.self, indexPath: indexPath)
            cell.versionLabel.text = "0.0.1"
            return cell
            
        } else if indexPath.row == 3 {
            let cell = tableView.register(NotificationSwitchTableViewCell.self, indexPath: indexPath)
            cell.configCell(isNotificationOn: isNotificationOn)
            return cell
        }
        let cell = tableView.register(SettingTableViewCell.self, indexPath: indexPath)
        cell.titleLabel.text = settingData[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            self.navigationController?.pushViewController(EditProfileViewController(), animated: true)
            
        case 1:
            self.navigationController?.pushViewController(AppRatingViewController(), animated: true)
            break
            
        case 2:
            self.navigationController?.pushViewController(DetectionViewController(), animated: true)
            break
            
        case 3:
            var setStatus = 1
            
            if isNotificationOn { setStatus = 0 }
            
            PostApi.setNotificationStatus(status: setStatus) { (data,error, statusCode) in
                
                if error == nil {
                    
                    self.isNotificationOn.toggle()
                    self.tableView.reloadData()
                    
                    if setStatus == 0 {
                        self.showOkAlert("Your app will not receive notifications now.")
                    
                    } else {
                        self.showOkAlert("Your app will receive notifications now.")
                    }
                    
                } else {
                    self.showOkAlert("Error! Please try again.")
                }
            }
            break
            
        default:
            break;
        }
    }
}
