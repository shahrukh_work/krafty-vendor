//
//  FAQsViewController.swift
//  CleaningPal
//
//  Created by Mac on 21/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class FAQsViewController: UIViewController {

    
    //MARK: - Outlet
    @IBOutlet weak var tableView: UITableView!
    

    //MARK: - Variable
    var data: [FAQ] = []
    

    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView ()
    }
    
    
    //MARK: - Setup
    func setupView () {
        tableView.delegate = self
        tableView.dataSource = self
        //self.tabBarController?.tabBar.isHidden = true
        dataSource ()
    }
    
    
    //MARK: - DataSource
    func dataSource () {
        data.append(FAQ(title: "Question number 01", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet."))
         data.append(FAQ(title: "Question number 02", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet."))
         data.append(FAQ(title: "Question number 03", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet."))
         data.append(FAQ(title: "Question number 04", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet."))
         data.append(FAQ(title: "Question number 05", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet."))
    }
    
    //MARK: - Actions
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - Methods
}


//MARK: - UITableViewDelegate & UITableViewDataSource
extension FAQsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(QuestionTableViewCell.self, indexPath: indexPath)
        cell.selectionStyle = .none
        cell.indexPath = indexPath
        cell.delegate = self
        cell.configure(faq: data[indexPath.row])
        return cell
    }
}


//MARK: - FAQDelegate
extension FAQsViewController: FAQDelegate {
    
    func showDescription(indexPath: IndexPath) {
        data[indexPath.row].isCollapsed.toggle()
        self.tableView.reloadData()
    }
}

