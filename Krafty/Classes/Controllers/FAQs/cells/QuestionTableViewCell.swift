//
//  QuestionTableViewCell.swift
//  CleaningPal
//
//  Created by Mac on 21/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
//import ExpandableLabel

protocol FAQDelegate: AnyObject {
    func showDescription (indexPath: IndexPath)
}

class QuestionTableViewCell: UITableViewCell {

    
    //MARK: - Outlet
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    

    //MARK: - Variable
    var indexPath: IndexPath?
    weak var delegate: FAQDelegate?

    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - configure
    func configure (faq: FAQ) {
        titleLabel.text = faq.title
        arrowImageView.image = #imageLiteral(resourceName: "ic_downarrow")
        
        if !faq.isCollapsed {
            descriptionLabel.text = faq.description
            arrowImageView.transform = CGAffineTransform(rotationAngle: .pi)
            
        } else {
            descriptionLabel.text = ""
        }
        
        self.layoutIfNeeded()
    }
    
    
    //MARK: - Actions
    @IBAction func collapsePressed(_ sender: Any) {
        delegate?.showDescription(indexPath: indexPath!)
    }
    
    
    //MARK: - Methods
}
