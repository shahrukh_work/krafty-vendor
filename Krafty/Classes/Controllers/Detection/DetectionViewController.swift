//
//  DetectionViewController.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/27/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class DetectionViewController: UIViewController {

    
    //MARK: - Outlets
    @IBOutlet weak var accountView: UIView!
    @IBOutlet weak var curvedView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var accoutDownView: UIView!
    @IBOutlet weak var detectionDownView: UIView!
    @IBOutlet weak var accountButton: UIButton!
    @IBOutlet weak var detectionButton: UIButton!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var cardNumberTextField: UITextField!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var cvcTextField: AppUITextField!
    @IBOutlet weak var nameTextField: AppUITextField!
    
    
    //MARK: - Variables
    var cardExpiryMonth = ""
    var cardExpiyYear = ""
    let datePickerView = UIPickerView()
    var months = [String]()
    var years = [String]()
    
    
    //MARK: - ViewController  Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
    //MARK: - View Setup
    private func setupView() {
        months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]
        let currentDate = Date()
        let currentYear = Calendar.current.component(.year, from: currentDate)
        let expiryRangeYear: Int = currentYear + 30
        for year in currentYear..<expiryRangeYear {
            years.append(String(year))
        }
        
        dateTextField.inputView = datePickerView
        datePickerView.delegate = self
        datePickerView.dataSource = self
        dateTextField.delegate = self
        
        tableView.delegate = self
        tableView.dataSource = self
        cardNumberTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.showHideArrows(isAccount: true)
    }
    
    
    //MARK: - Actions
    @IBAction func detectionPressed(_ sender: Any) {
        self.showHideArrows(isAccount: false)
    }
    
    @IBAction func accountPressed(_ sender: Any) {
        self.showHideArrows(isAccount: true)
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clearButtonTapped(_ sender: Any) {
        nameTextField.text = ""
        cardNumberTextField.text = ""
        dateTextField.text = ""
        cardExpiryMonth = ""
        cardExpiyYear = ""
        cvcTextField.text = ""
    }
    
    @IBAction func minusButtonTapped(_ sender: Any) {
        cardNumberTextField.text = ""
        minusButton.isHidden = true
    }
    
    @IBAction func updateButtonTapped(_ sender: Any) {
        
        if nameTextField.text == "" {
            self.showOkAlert("Please enter your name.")
            
        } else if cardNumberTextField.text?.count ?? 0 < 8 {
            self.showOkAlert("Please enter a valid card number.")
        
        } else if dateTextField.text == "" {
            self.showOkAlert("Please enter your card's expiry date.")
        
        } else if cvcTextField.text == "" {
            self.showOkAlert("Please enter your CVC.")
        
        } else {
            //TODO: - update button functionality
        }
        
    }
    
    
    //MARK: - Methods
    func showHideArrows (isAccount: Bool) {
        
        if isAccount {
            
            UIView.animate(withDuration: 0.2, animations: {
                self.accountView.alpha = 1.0
                self.accountView.isHidden = false
                self.detectionDownView.alpha = 0.0
                self.accoutDownView.alpha = 1.0
                self.view.layoutIfNeeded()
            })
            
        } else {
            
            UIView.animate(withDuration: 0.2, animations: {
                self.accountView.alpha = 0.0
                self.accountView.isHidden = true
                self.detectionDownView.alpha = 1.0
                self.accoutDownView.alpha = 0.0
                self.view.layoutIfNeeded()
            })
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text == "" {
            minusButton.isHidden = true
            
        } else {
            minusButton.isHidden = false
        }
    }
}


//MARK: - UITableViewDelegate & UITableViewDataSource
extension DetectionViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        12
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(DetectionTableViewCell.self, indexPath: indexPath)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //TODO: - tableView cell selection
    }
}


//MARK: - PickerView Delegate & DataSource
extension DetectionViewController: UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return months.count
            
        } else {
            return years.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return months[row]
            
        } else {
            return years[row]
        }
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0 {
            cardExpiryMonth = months[row]
            
        } else {
            cardExpiyYear = years[row]
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if cardExpiryMonth != "" && cardExpiyYear != "" {
            dateTextField.text = "\(cardExpiryMonth) / \(cardExpiyYear)"
        }
    }
}
