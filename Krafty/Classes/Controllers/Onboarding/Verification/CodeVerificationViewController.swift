//
//  CodeVerificationViewController.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/23/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class CodeVerificationViewController: UIViewController, UITextFieldDelegate {

    
    //MARK: - Variables
    var isForSignUp = false
    
    
    //MARK: - Outlets
    @IBOutlet weak var textField1: UITextField!
    @IBOutlet weak var textField2: UITextField!
    @IBOutlet weak var textField3: UITextField!
    @IBOutlet weak var textField4: UITextField!
    @IBOutlet weak var verifyButton: UIButton!
    @IBOutlet weak var emailInfoLabel: UILabel!
    
    
    //MARK: - Variables
    var email = ""
    var verificationPin = ""
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
    //MARK: - Actions
    @IBAction func verifyButtonTapped(_ sender: Any) {
        
        PostApi.verifyPin(email: email, pin: verificationPin) {(data, error, message) in
            
            if error == nil {
                
                if self.isForSignUp {
                    Utility.setupHomeAsRootViewController()
                    
                } else {
                    let controller = ChangePasswordViewController()
                    controller.token = data!.token
                    self.navigationController?.pushViewController(controller, animated: true)
                }
                
            } else {
                self.showOkAlert("Invalid pin.")
            }
        }
    }
    
    @IBAction func editMobileNumberButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resendCodeButtonTapped(_ sender: Any) {
        
        PostApi.resendVerificationPin(email: email) {(data, error, statusCode) in
            
            if error == nil {
                self.showOkAlert("Verification pin has been resent.")
                
            } else {
                self.showOkAlert("Verification pin could not be resent.")
            }
        }
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - View Setup
    private func setupView() {
        emailInfoLabel.halfTextColorChange(color: #colorLiteral(red: 0.564625144, green: 0.9315071702, blue: 0.5658783317, alpha: 1), fullText: "Enter the 4-digit code sent to you at \(email)", changeText: email)
        
        textField1.delegate = self
        textField2.delegate = self
        textField3.delegate = self
        textField4.delegate = self
        
        textField2.isUserInteractionEnabled = false
        textField3.isUserInteractionEnabled = false
        textField4.isUserInteractionEnabled = false
        
        textField1.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField2.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField3.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField4.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        (self.view.viewWithTag(1001) as! UITextField).becomeFirstResponder()
    }
    
    
    //MARK: - UITextFieldDelegate
    @objc func textFieldDidChange(_ textField: UITextField) {
        let tag = textField.tag
        textField.backgroundColor = #colorLiteral(red: 0.564625144, green: 0.9315071702, blue: 0.5658783317, alpha: 1)
        if tag == 1004 {
            let verificationCode = textField1.text! + textField2.text! + textField3.text! + textField4.text!
            textField.resignFirstResponder()
            
            if verificationCode.count == 4 {
                verifyButton.isUserInteractionEnabled = true
                verifyButton.backgroundColor = #colorLiteral(red: 0.564625144, green: 0.9315071702, blue: 0.5658783317, alpha: 1)
                verificationPin = verificationCode
            
            } else {
                verifyButton.isUserInteractionEnabled = false
                verifyButton.backgroundColor = #colorLiteral(red: 0.5647058824, green: 0.9333333333, blue: 0.5647058824, alpha: 0.8376230736)
            }
        
        } else {
            let text = textField.text!
            
            if text != "" {
                let lastChar = String(describing: Array(text)[text.count - 1])
                textField.text = lastChar + ""
                (self.view.viewWithTag(tag+1) as! UITextField).isUserInteractionEnabled = true
                (self.view.viewWithTag(tag+1) as! UITextField).becomeFirstResponder()
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            textField.backgroundColor = #colorLiteral(red: 0.9098039216, green: 0.9098039216, blue: 0.9098039216, alpha: 1)
            verifyButton.isUserInteractionEnabled = false
            verifyButton.backgroundColor = #colorLiteral(red: 0.5411764706, green: 0.7882352941, blue: 1, alpha: 1)
            textField.isUserInteractionEnabled = false
            textField.text = ""
            
            if textField.tag != 1001
            {
                (self.view.viewWithTag(textField.tag-1) as! UITextField).becomeFirstResponder()
                return false
            }
        }
        return true
    }
}
