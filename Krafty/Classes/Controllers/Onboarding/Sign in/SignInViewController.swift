//
//  SignInViewController.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/22/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController {

    
    //MARK: - Outlets
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    var enteredPassword = ""
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    //MARK: - View Setup
    private func setupView() {
        passwordTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
    }
    
    
    //MARK: - Actions
    @IBAction func signInButtonTapped(_ sender: Any) {
        
        if isValidInformation() {
            
            Profile.signIn(email: userNameTextField.text!, password: enteredPassword) { (data,error,message, statusCode) in
                
                if error == nil {
                    
                    if message == "PAUSED" {
                        self.showOkAlert( "Your user has been paused by the admin")
                        
                    } else if message == "DELETED" {
                         self.showOkAlert("Your user has been deleted by the admin")
                        
                    } else if message == "PENDING" {
                        let controller = CodeVerificationViewController()
                        controller.isForSignUp = true
                        self.navigationController?.pushViewController(controller, animated: true)
                        
                    } else {
                        User.shared.profile = data
                        UserDefaults.standard.set(User.shared.profile?.toJSONString(), forKey: "UserDefaults")
                        Utility.setupHomeAsRootViewController()
                    }

                } else {
                    self.showOkAlert("Login Failed")
                }
            }
        }
    }
    
    @IBAction func forgotPasswordButtonTapped(_ sender: Any) {
        let controller = ForgotPasswordViewController()
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func signUpButtonTapped(_ sender: Any) {
        let controller = SignUpViewController()
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    //MARK: - Selectors
    @objc func textFieldDidChange(_ textField: UITextField) {
        let text = textField.text ?? ""
        
        if text.count > enteredPassword.count { // entered something
            enteredPassword += String(text.last!)
            
        } else { // cleared something
            enteredPassword.removeLast()
        }
        
        var stericks = ""
        for _ in 0..<text.count {
            stericks += "●"
        }
        textField.text = stericks // show stericks to the user instead of password
    }
    
    
    //MARK: - Private Methods
    private func isValidInformation () -> Bool {
        var message = ""
        
        if userNameTextField.text == "" {
            message = "Please provide your username or email."
        }
        
        if enteredPassword == "" {
            message = "Please enter your password."
        }
        
        if message != "" {
            self.showOkAlert(message)
            return false
        }
        return true
    }
}
