//
//  ChangePasswordViewController.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/23/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {

    
    //MARK: - Outlets
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmNewPasswordTextField: UITextField!
    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var oldPasswordView: UIView!
    
    
    //MARK: - Variables
    var enteredOldPassword = ""
    var enteredNewPassword = ""
    var enteredConfirmPassword = ""
    var isForChangingPassword = false
    var token = ""
    
    
    //MARK: - ViewController  Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
    //MARK: - View Setup
    private func setupView() {
        
        if isForChangingPassword {
            oldPasswordView.isHidden = false
            oldPasswordTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        }
        
        newPasswordTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        confirmNewPasswordTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
    }
    
    
    //MARK: - Private Methods
    private func updatePassword() {
        
        PostApi.updatePassword(token: token, password: enteredNewPassword) { (data, error, statusCode) in
            
            if error == nil {
                let controller = SignInViewController()
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    private func changePassword() {
        
        PostApi.changePassword(oldPassword: enteredOldPassword, newPassword: enteredNewPassword) { (data, error, statusCode) in
            
            if error == nil {
                self.oldPasswordTextField.text = ""
                self.newPasswordTextField.text = ""
                self.confirmNewPasswordTextField.text = ""
                self.showOkAlert("You password has been updated.")
            }
        }
    }
    
    
    //MARK: - Actions
    @IBAction func updatePasswordButtonTapped(_ sender: Any) {
        
        if isForChangingPassword {
            if enteredOldPassword == "" {
                self.showOkAlert("Please provide your old password.")
                return
            }
        }
        
        if enteredNewPassword == "" {
            self.showOkAlert("Please provide a new valid password.")
            
        } else if enteredNewPassword != enteredConfirmPassword {
            self.showOkAlert("Passwords do not match.")
            
        } else {
            
            // if the user wants to change password, he/she will 'change' password by entering old password
            if isForChangingPassword {
                changePassword()
               
            // if the user has forgotten password, he/she will 'update' password by providing token and new password
            } else {
                updatePassword()
            }
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Selectors
    @objc func textFieldDidChange(_ textField: UITextField) {
        let text = textField.text ?? ""
        
        if textField == newPasswordTextField {
            
            if text.count > enteredNewPassword.count { // entered something
                enteredNewPassword += String(text.last!)
                
            } else { // cleared something
                enteredNewPassword.removeLast()
            }
            
        } else if textField == confirmNewPasswordTextField {
            
            if text.count > enteredConfirmPassword.count { // entered something
                enteredConfirmPassword += String(text.last!)
                
            } else { // cleared something
                enteredConfirmPassword.removeLast()
            }
        
        } else if textField == oldPasswordTextField {
            
            if text.count > enteredOldPassword.count { // entered something
                enteredOldPassword += String(text.last!)
                
            } else { // cleared something
                enteredOldPassword.removeLast()
            }
        }
        
        var stericks = ""
        for _ in 0..<text.count {
            stericks += "●"
        }
        textField.text = stericks // show stericks to the user instead of password
    }
}
