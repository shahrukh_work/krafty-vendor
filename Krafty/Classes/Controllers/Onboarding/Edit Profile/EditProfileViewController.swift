//
//  EditProfileViewController.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/24/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import SDWebImage
import ObjectMapper

class EditProfileViewController: UIViewController {

    
    //MARK: - Outlets
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var dateOfBirthField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var changePasswordButton: UIButton!
    
    
    //MARK: - Variables
    let datePickerView:UIDatePicker = UIDatePicker()
    var imagePicker = UIImagePickerController()
    var profileImage: UIImage?
    
    
    //MARK: - ViewController  Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
    //MARK: - View Setup
    private func setupView() {
        imagePicker.delegate = self
        let attrs = [NSAttributedString.Key.font : UIFont.font(withSize: 15),
                     NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1),
                     NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
        let attributedString = NSMutableAttributedString(string:"")
        let buttonTitleStr = NSMutableAttributedString(string:"Change Password", attributes:attrs)
        attributedString.append(buttonTitleStr)
        changePasswordButton.setAttributedTitle(attributedString, for: .normal)
        
        fetchProfile()
    }
    
    
    //MARK: - Actions
    @IBAction func saveChangesButtonTapped(_ sender: Any) {
        let userName = userNameTextField.text
        let firstName = firstNameTextField.text
        let lastName = lastNameTextField.text
        let email = emailTextField.text
        
        if userName == "" {
            self.showOkAlert("Please provide a username.")
            
        } else if firstName == "" {
            self.showOkAlert("Please provide your first name.")
            
        } else if lastName == "" {
            self.showOkAlert("Please provide your last name.")
            
        } else if !Utility.isValidEmail(emailStr: email!) {
            self.showOkAlert("Please provide a valid email.")
            
        } else {
            let profile = Mapper<ProfileData>().map(JSON:[:])!
            profile.id = (User.shared.profile?.profileData!.id)!
            profile.userName = userName!
            profile.firstName = firstName!
            profile.lastName = lastName!
            profile.email = email!
            
            Utility.showLoading()
            APIClient.shared.updateProfileData(profile: profile, profileImage: profileImage) { (data, error, statusCode) in
                Utility.hideLoading()
                
                if error == nil { // success
                    self.fetchProfile(profileUpdated: true)
                
                } else { // failure
                    self.showOkAlert("Something went wrong")
                }
            }
        }
    }
    
    @IBAction func changePasswordButtonTapped(_ sender: Any) {
        let controller = ChangePasswordViewController()
        controller.isForChangingPassword = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func addImageButtonTapped(_ sender: Any) {
        //TODO: - select image from gallery
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "MMM, d, yyyy"
        dateOfBirthField.text = dateFormatter.string(from: sender.date)
    }
    
    
    //MARK: - Private Methods
    func fetchProfile(profileUpdated: Bool = false) {
        
        Profile.getUserDetails(userId: (User.shared.profile?.profileData?.id)!) { (data, error, message) in
            
            if error == nil {
                User.shared.profile?.profileData = data?.profileData
                let tempProfileData = User.shared.profile?.profileData
                self.userNameLabel.text = "\(tempProfileData?.firstName ?? "") \(tempProfileData?.lastName ?? "")"
                self.userNameTextField.text = tempProfileData?.userName
                self.firstNameTextField.text = tempProfileData?.firstName
                self.lastNameTextField.text = tempProfileData?.lastName
                self.emailTextField.text = tempProfileData?.email
                self.profileImageView.sd_setImage(with: URL(string: APIRoutes.profileImageUrl + tempProfileData!.image), placeholderImage: UIImage(named: "male "))
                UserDefaults.standard.set(User.shared.profile?.toJSONString(), forKey: "UserDefaults")
                if profileUpdated {
                    self.showOkAlert("Profile updated successfully")
                }
                
            } else {
                self.showOkAlert(kErrorMessage)
            }
        }
    }
}


extension EditProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            profileImageView.image = image
            profileImage = image
        }
        dismiss(animated: true, completion: nil)
    }
}
