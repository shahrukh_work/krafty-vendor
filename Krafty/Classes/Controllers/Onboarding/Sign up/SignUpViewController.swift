//
//  SignUpViewController.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/22/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import ObjectMapper

class SignUpViewController: UIViewController {

    
    //MARK: - Outlets
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var serviceTypeTextField: UITextField!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var etaNumberTextField: UITextField!
    
    
    //MARK: - Variables
    var genderPickerView = UIPickerView()
    var genders = ["Male", "Female"]
    var services = [Service]()
    var serviceId = -1
    let servicePickerView = UIPickerView()
    var enteredPassword = ""
    var enteredConfirmPassword = ""
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
    //MARK: - View Setup
    private func setupView() {
        passwordTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        confirmPasswordTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        bottomView.layer.cornerRadius = 18
        bottomView.layer.maskedCorners = [.layerMaxXMinYCorner]
        
        genderTextField.inputView = genderPickerView
        genderPickerView.delegate = self
        genderPickerView.dataSource = self
        genderTextField.delegate = self
        
        Services.getServicesList() { (data, error, message) in
            if error == nil {
                self.services = data!.servicesList
                self.serviceTypeTextField.inputView = self.servicePickerView
                self.servicePickerView.delegate = self
                self.servicePickerView.dataSource = self
                self.serviceTypeTextField.delegate = self
                
            } else {
                self.showOkAlert(kErrorMessage)
            }
        }
    }
    
    
    //MARK: - Actions
    @IBAction func signUpButtonTapped(_ sender: Any) {
        
        if isValidInformation() {
            let userName = userNameTextField.text!
            let firstName = firstNameTextField.text!
            let lastName = lastNameTextField.text
            let gender = genderTextField.text
            let age = ageTextField.text
            let email = emailTextField.text
            let phone = phoneTextField.text
            let etaNumber = etaNumberTextField.text
            
            Profile.signUp(username: userName, firstName: firstName, lastName: lastName!, gender: gender!, age: age!, email: email!, password: enteredPassword, phone: phone!, serviceType: String(serviceId), etaNumber: etaNumber!) { (data, error, message) in
                
                if error == nil {
                    let controller = CodeVerificationViewController()
                    controller.isForSignUp = true
                    controller.email = email!
                    self.navigationController?.pushViewController(controller, animated: true)
                    
                } else {
                    self.showOkAlert(kErrorMessage)
                }
            }
        }
    }
    
    @IBAction func signInButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - Selectors
    @objc func textFieldDidChange(_ textField: UITextField) {
        let text = textField.text ?? ""
        
        if textField == passwordTextField {
            
            if text.count > enteredPassword.count { // entered something
                enteredPassword += String(text.last!)
                
            } else { // cleared something
                enteredPassword.removeLast()
            }
            
        } else if textField == confirmPasswordTextField {
            
            if text.count > enteredConfirmPassword.count { // entered something
                enteredConfirmPassword += String(text.last!)
                
            } else { // cleared something
                enteredConfirmPassword.removeLast()
            }
        }
        
        var stericks = ""
        for _ in 0..<text.count {
            stericks += "●"
        }
        textField.text = stericks // show stericks to the user instead of password
    }
    
    
    //MARK: - Private Methods
    private func isValidInformation () -> Bool {
        var message = ""
        
        if userNameTextField.text == "" {
            message = "User name is empty! Please type user name."
        }
        
        if firstNameTextField.text == "" {
            message = "First name is empty! Please type first name."
        }
        
        if lastNameTextField.text == "" {
            message = "Last name is empty! Please type Last name."
        }
        
        if genderTextField.text == "" {
            message = "Please specify your gender."
        }
        
        if ageTextField.text == "" {
            message = "Please provide your age."
        }
        
        if emailTextField.text == "" {
            message = "Email is empty! Please type valid email."
        }
        
        if enteredPassword == "" {
            message = "Password is empty! Please type password."
        }
        
        if enteredConfirmPassword == "" {
            message = "Confirm password is empty! Please type confirm password."
        }
        
        if enteredConfirmPassword != enteredPassword {
            message = "Confirm password does not match! Please type valid confirm password."
        }
        
        if !Utility.isValidEmail(emailStr: emailTextField.text ?? "") {
            message = "Email is not valid! Please type valid email."
        }
        
        if !Utility.isPhoneNumberValid(value: phoneTextField.text ?? "") {
            message = "Please provide a valid phone number."
        }
        
        if etaNumberTextField.text == "" {
            message = "Please provide your ETA number."
        }
        
        if message != "" {
            self.showOkAlert(message)
            return false
        }
        return true
    }
}

//MARK: - PickerView Delegate & DataSource
extension SignUpViewController: UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == genderPickerView {
            return genders.count
        
        } else {
            return services.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == genderPickerView {
            return genders[row]
            
        } else {
            return services[row].name
        }
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == genderPickerView {
            genderTextField.text = genders[row]
            
        } else {
            serviceTypeTextField.text = services[row].name
            serviceId = services[row].id
        }
    }
}
