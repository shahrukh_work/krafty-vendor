//
//  HistoryListViewController.swift
//  Krafty
//
//  Created by Mac on 28/04/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class HistoryListViewController: UIViewController {

    
    //MARK: - Outlet
    @IBOutlet weak var tableView: UITableView!
    
    
    
    //MARK: - Controller Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup ()
    }
    
    
    //MARK: - Setup
    func setup () {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    //MARK: - Actions
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}


//MARK: - UITableViewDelegate & UITableViewDataSource
extension HistoryListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(HistoryTableViewCell.self, indexPath: indexPath)
        cell.selectionStyle = .none
        return cell
    }
    
    
}
