//
//  SubService.swift
//  Krafty-Vendor
//
//  Created by Mian Faizan Nasir on 6/19/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

class SubService: Mappable {
    var id = ""
    var name = ""
    var serviceId = ""
    var isSelected = false
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        serviceId <- map["service_id"]
    }
}
