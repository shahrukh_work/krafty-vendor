//
//  Rating.swift
//  Krafty-Vendor
//
//  Created by Bilal Saeed on 8/4/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper


typealias RatingsCompletionHandler = (_ data: Rating?,_ error: NSError?,_ message: String) -> Void

class Rating: Mappable {
    
    var stars = -1.1
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        stars    <- map["stars"]
    }
    
    class func getRatings(completion: @escaping RatingsCompletionHandler) {
        Utility.showLoading()

        APIClient.shared.getRatings { (data, error, message) in
            Utility.hideLoading()

            if error == nil {
                if let data = Mapper<Rating>().map(JSONObject: data) {
                    completion(data, nil, "")

                } else {
                    completion(Mapper<Rating>().map(JSON: [:]), nil, "")
                }

            } else {
                completion(Mapper<Rating>().map(JSON: [:])!, error, "Error!")
            }
        }
    }
}

