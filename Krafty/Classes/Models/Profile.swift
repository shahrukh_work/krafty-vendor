//
//  SignUp.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 5/29/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire

typealias SignInCompletionHandler = (_ data: Profile?,_ error: NSError?,_ message: String?,_ statusCode: Int) -> Void
typealias SignUpCompletionHandler = (_ data: Profile?,_ error: NSError?,_ message: String) -> Void

class Profile: Mappable {
    var status = ""
    var token = ""
    var fcmToken = ""
    var profileData = Mapper<ProfileData>().map(JSON: [:])
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        status              <- map["status"]
        profileData                <- map["user"]
        token               <- map["token"]
    }
    
        class func signIn(email: String, password: String,  completion: @escaping SignInCompletionHandler) {
    
            Utility.showLoading()
    
            APIClient.shared.signIn(email: email, password: password) { (data, error, statusCode) in
    
                Utility.hideLoading()
    
                if error == nil {
    
                    if let data = data {
    
                        if let d = data as? [String: AnyObject] {
    
                            let status = d["status"] as! String
                            if status == "PAUSED" {
                                completion(Mapper<Profile>().map(JSON: [:])!, nil, "PAUSED", statusCode)
    
                            } else if status == "DELETED" {
                                completion(Mapper<Profile>().map(JSON: [:])!, nil, "DELETED", statusCode)
    
                            } else if status == "PENDING" {
                                completion(Mapper<Profile>().map(JSON: [:])!, nil, "PENDING", statusCode)
                                
                            } else {
    
                                if let dataProfile = Mapper<Profile>().map(JSONObject: data) {
    
                                    PostApi.updateFCMToken(token: User.shared.profile?.fcmToken ?? "") { (message) in
                                        print(message ?? "")
                                    }
                                    completion(dataProfile, nil, "login completed", statusCode)
    
                                } else {
    
                                }
                            }
                        }
                    }
    
                }  else {
                    print(error?.localizedDescription ?? "")
                    completion(Mapper<Profile>().map(JSON: [:])!, error, "", statusCode)
                }
            }
        }
    
    class func signUp(username:String, firstName: String, lastName: String, gender: String, age: String, email: String, password: String, phone: String, serviceType: String, etaNumber: String,  completion: @escaping SignUpCompletionHandler) {
        
        Utility.showLoading()
        
        APIClient.shared.signUp(username: username, firstName: firstName, lastName: lastName, gender: gender, age: age, email: email, password: password, phone: phone, serviceType: serviceType, etaNumber: etaNumber) { (data, error, statusCode) in
        
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = Mapper<Profile>().map(JSONObject: data) {
                    completion(data, nil, "")
                    
                } else {
                    completion(Mapper<Profile>().map(JSON: [:])!, nil, "")
                }
                
            }  else {
                print(error?.localizedDescription ?? "")
                completion(Mapper<Profile>().map(JSON: [:])!, error, "There was an error. Pleas try again.")
            }
        }
    }
    
    class func getUserDetails(userId: Int,  completion: @escaping SignUpCompletionHandler) {

        Utility.showLoading()

        APIClient.shared.getUserDetails(userId: userId) { (data, error, statusCode) in

            Utility.hideLoading()

            if error == nil {
                if let data1 = Mapper<Profile>().map(JSONObject: data) {
                    completion(data1, nil, "")

                } else {
                    completion(Mapper<Profile>().map(JSON: [:])!, nil, "")
                }

            }  else {
                print(error?.localizedDescription ?? "")
                completion(Mapper<Profile>().map(JSON: [:])!, error, "There was an error. Pleas try again.")
            }
        }
    }
}

class ProfileData: Mappable {
    var id = -1
    var userName = ""
    var firstName = ""
    var lastName = ""
    var email = ""
    var image = ""
    var status = ""
    var verification_pin = ""
    var createdAt = ""
    var updatedAt = ""
    var notification = -1
    var token = ""
    var dob = ""
    var gender = ""
    var phone = ""
    var location = ""
    var age = ""
    var socketId = -1
    var serviceType = -1
    var etaNumber = ""
    var fcmToken = ""
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        userName <- map["username"]
        firstName <- map["first_name"]
        lastName <- map["last_name"]
        email <- map["email"]
        image <- map["image"]
        status <- map["status"]
        verification_pin <- map["verification_pin"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        notification <- map["notification"]
        dob <- map["dob"]
        gender <- map["gender"]
        phone <- map["phone"]
        location <- map["location"]
        age <- map["age"]
        socketId <- map["socket_id"]
        serviceType <- map["service_type"]
        etaNumber <- map["eta_number"]
        fcmToken <- map["fcm_token"]
    }
}
