//
//  ServiceTypeData.swift
//  Krafty-Vendor
//
//  Created by Mian Faizan Nasir on 6/19/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation

class ServiceTypeData {
    
    var title = ""
    var isSelected = false
    
    init(title: String, isSelected: Bool = false) {
        self.title = title
        self.isSelected = isSelected
    }
}
