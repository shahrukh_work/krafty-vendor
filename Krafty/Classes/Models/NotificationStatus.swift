//
//  NotificationStatus.swift
//  Krafty-Vendor
//
//  Created by Mian Faizan Nasir on 6/8/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias NotificationStatusCompletionHandler = (_ data: NotificationStatus?,_ error: NSError?,_ message: String) -> Void

class NotificationStatus: Mappable {
    var status = -1
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        status <- map["notification"]
    }
    
    class func getNotificationStatus(completion: @escaping NotificationStatusCompletionHandler) {
        
        Utility.showLoading()
        
        APIClient.shared.getNotificationStatus() { (data, error, message) in
            Utility.hideLoading()
            
            if error == nil {
                if let data = Mapper<NotificationStatus>().map(JSONObject: data) {
                    completion(data, nil, "")
                
                } else {
                    completion(Mapper<NotificationStatus>().map(JSON: [:])!, nil, "")
                }
            
            } else {
                completion(Mapper<NotificationStatus>().map(JSON: [:])!, error, "Error!")
            }
        }
    }
}
