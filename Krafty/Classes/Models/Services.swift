//
//  Services.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 6/2/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias ServicesCompletionHandler = (_ data: Services?,_ error: NSError?,_ message: String) -> Void

class Services: Mappable {
    var servicesList = [Service]()
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        servicesList <- map["services"]
    }
    
    class func getServicesList(completion: @escaping ServicesCompletionHandler) {
        
        Utility.showLoading()
        
        APIClient.shared.getServicesList() { (data, error, message) in
        
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = Mapper<Services>().map(JSONObject: data) {
                    completion(data, nil, "")
                    
                } else {
                    completion(Mapper<Services>().map(JSON: [:])!, nil, "")
                }
                
            }  else {
                print(error?.localizedDescription ?? "")
                completion(Mapper<Services>().map(JSON: [:])!, error, "There was an error. Pleas try again.")
            }
        }
    }
}

class Service: Mappable {
    var id = -1
    var name = ""
    var image = ""
    var description = ""
    var createdAt = ""
    var updatedAt = ""
    var isSelected = false
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        image <- map["image"]
        description <- map["description"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
    }
}

