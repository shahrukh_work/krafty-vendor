//
//  ChatHistory.swift
//  Krafty-Vendor
//
//  Created by Bilal Saeed on 6/11/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias ChatCompletionHandler = (_ data: ChatHistory?, _ error: NSError?) -> Void

class ChatHistory: Mappable {
    
    var chatHistory = [Chat]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        chatHistory <- map["chat_history"]
    }
    
    class func loadChat(orderId: Int, completion: @escaping ChatCompletionHandler) {
        APIClient.shared.chatHistory(orderId: orderId) { (data, error, status) in
            
            if error == nil {
                
                if let data = Mapper<ChatHistory>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(Mapper<ChatHistory>().map(JSON: [:]), nil)
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}

class Chat: Mappable {
    
    var id = -1
    var orderID = -1
    var message = ""
    var sendBy = -1
    var createdAt = ""
    var updateAt = ""
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id          <- map["id"]
        orderID     <- map["order_id"]
        message     <- map["message"]
        sendBy      <- map["send_by"]
        createdAt   <- map["created_at"]
        updateAt    <- map["updated_at"]
    }
}
