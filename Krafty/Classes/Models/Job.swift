//
//  Job.swift
//  Krafty-Vendor
//
//  Created by Bilal Saeed on 6/10/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper
import UIKit


class Job: Mappable {
    
    var customerImage = ""
    var customerName = ""
    var id = -1
    var customerId = -1
    var orderAddress = ""
    private var orderImageString = ""
    private var latLongString = ""
    private var userOptionsString = ""
    var orderImages = [String]()
    var userOptions = [String]()
    var orderDetails = ""
    var lat = ""
    var long = ""
    var stars = 0.0
    var status = 0
    
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        customerImage       <- map["customer_image"]
        customerName        <- map["customer_username"]
        id                  <- map["id"]
        customerId          <- map["customer_id"]
        orderAddress        <- map["order_address"]
        orderImageString    <- map["order_images"]
        latLongString       <- map["order_location"]
        userOptionsString   <- map["user_options"] //TODO: - To be updated key missing from backend
        orderDetails        <- map["order_detail"]
        stars               <- map["stars"]
        status              <- map["status"]
        
        postMap()
    }
    
    func postMap() {
        orderImages = orderImageString.components(separatedBy: ",")
        userOptions = userOptionsString.components(separatedBy: ",")
        let latLong = latLongString.components(separatedBy: ",")
        lat = latLong.first ?? ""
        long = latLong.last ?? ""
    }
}
