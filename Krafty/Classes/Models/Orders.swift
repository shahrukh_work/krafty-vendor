//
//  Orders.swift
//  Krafty-Vendor
//
//  Created by Mian Faizan Nasir on 6/9/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias OrdersCompletionHandler = (_ data: Orders?,_ error: NSError?,_ message: String) -> Void

class Orders: Mappable {
    var completedOrdersList = [Order]()
    var proceedingOrdersList = [Order]()
    var currentOrdersList = [Order]()
    var historyOrdersList = [Order]()
    var allOrders = [Order]()
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        completedOrdersList     <- map["completed_jobs"]
        proceedingOrdersList    <- map["proceeding_jobs"]
        currentOrdersList       <- map["current_order"]
        historyOrdersList       <- map["order_history"]
        allOrders               <- map["orders"]
    }
    
    class func getProceedingOrders(completion: @escaping OrdersCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.getProceedingOrders() { (data, error, message) in
            
            Utility.hideLoading()
            
            if error == nil {
                if let data = Mapper<Orders>().map(JSONObject: data) {
                    completion(data, nil, "")
                    
                } else {
                    completion(Mapper<Orders>().map(JSON: [:]), nil, "")
                }
            
            } else {
                completion(Mapper<Orders>().map(JSON: [:])!, error, "Error!")
            }
        }
    }
    
    class func getCompletedOrders(completion: @escaping OrdersCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.getCompletedOrders() { (data, error, message) in
            Utility.hideLoading()
            
            if error == nil {
                if let data = Mapper<Orders>().map(JSONObject: data) {
                    completion(data, nil, "")
                    
                } else {
                    completion(Mapper<Orders>().map(JSON: [:]), nil, "")
                }
            
            } else {
                completion(Mapper<Orders>().map(JSON: [:])!, error, "Error!")
            }
        }
    }
    
    class func getCurrentOrders(completion: @escaping OrdersCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.getCurrentOrders() { (data, error, message) in
            
            Utility.hideLoading()
            
            if error == nil {
                if let data = Mapper<Orders>().map(JSONObject: data) {
                    completion(data, nil, "")
                    
                } else {
                    completion(Mapper<Orders>().map(JSON: [:]), nil, "")
                }
            
            } else {
                completion(Mapper<Orders>().map(JSON: [:])!, error, "Error!")
            }
        }
    }
    
    class func getOrdersHistory(completion: @escaping OrdersCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.getOrdersHistory() { (data, error, message) in
            Utility.hideLoading()
            
            if error == nil {
                if let data = Mapper<Orders>().map(JSONObject: data) {
                    completion(data, nil, "")
                    
                } else {
                    completion(Mapper<Orders>().map(JSON: [:]), nil, "")
                }
            
            } else {
                completion(Mapper<Orders>().map(JSON: [:])!, error, "Error!")
            }
        }
    }
    
    class func getAllOrders(completion: @escaping OrdersCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.getAllOrders() { (data, error, message) in
            Utility.hideLoading()
            
            if error == nil {
                if let data = Mapper<Orders>().map(JSONObject: data) {
                    completion(data, nil, "")
                    
                } else {
                    completion(Mapper<Orders>().map(JSON: [:]), nil, "")
                }
            
            } else {
                completion(Mapper<Orders>().map(JSON: [:])!, error, "Error!")
            }
        }
    }
}

class Order: Mappable {
    var id = -1
    var driverId = -1
    var driverLocation = ""
    var estimatedDistance = ""
    var customerId = -1
    var customerAddress = ""
    var customerLocation = ""
    var description = ""
    var serviceType = -1
    var status = -1
    var startDateTime = ""
    var endDateTime = ""
    var createdBy = ""
    var createdAt = ""
    var updatedAt = ""
    var customerName = ""
    var stars = 0.0
    var customerImage = ""
    var orderImages = ""
    var serviceName = ""
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        driverId <- map["driver_id"]
        driverLocation <- map["driver_location"]
        estimatedDistance <- map["estimated_distance"]
        customerId <- map["customer_id"]
        customerAddress <- map["customer_address"]
        customerLocation <- map["customer_location"]
        description <- map["order_detail"]
        serviceType <- map["serviceType"]
        status <- map["status"]
        startDateTime <- map["start_date_time"]
        endDateTime <- map["end_date_time"]
        createdBy <- map["created_by"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        customerName <- map["customer_name"]
        stars <- map["stars"]
        customerImage <- map["customer_image"]
        orderImages <- map["order_images"]
        serviceName <- map["service_name"]
    }
}
