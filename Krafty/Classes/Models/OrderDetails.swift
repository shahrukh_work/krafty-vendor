//
//  CurrentOrder.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 6/5/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias OrderDetailsCompletionHandler = (_ data: OrderDetails?,_ error: NSError?,_ message: String) -> Void

class OrderDetails: Mappable {
    var details = Mapper<Details>().map(JSON: [:])

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        details <- map["order_detail"]
    }

    class func getOrderDetails(orderId: Int, completion: @escaping OrderDetailsCompletionHandler) {
        Utility.showLoading()

        APIClient.shared.getOrderDetails(orderId: orderId) { (data, error, message) in
            Utility.hideLoading()

            if error == nil {
                if let data = Mapper<OrderDetails>().map(JSONObject: data) {
                    completion(data, nil, "")

                } else {
                    completion(Mapper<OrderDetails>().map(JSON: [:]), nil, "")
                }

            } else {
                completion(Mapper<OrderDetails>().map(JSON: [:])!, error, "Error!")
            }
        }
    }
}

class Details: Mappable {
    var id = -1
    var driverId = -1
    var customerId = -1
    var driverName = ""
    var customerImage = ""
    var stars = 0
    var customerName = ""
    var driverLocation = ""
    var estimatedDistance = ""
    var customerAddress = ""
    var customerLocation = ""
    var description = ""
    var serviceType = -1
    var serviceName = ""
    var status = -1
    var startDateTime = ""
    var endDateTime = ""
    var createdBy = ""
    var createdAt = ""
    var updatedAt = ""
    var images = ""
    var imageUrls = [String]() // this property is being used in controller after splittings 'images' property

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        id <- map["id"]
        driverId <- map["driver_id"]
        customerId <- map["customer_id"]
        customerImage <- map["customer_image"]
        stars <- map["stars"]
        driverName <- map["driver_name"]
        customerName <- map["customer_name"]
        driverLocation <- map["driver_location"]
        estimatedDistance <- map["estimated_distance"]
        customerAddress <- map["customer_address"]
        customerLocation <- map["customer_location"]
        description <- map["order_detail"]
        serviceType <- map["service_type"]
        serviceName <- map["service_name"]
        status <- map["status"]
        startDateTime <- map["start_date_time"]
        endDateTime <- map["end_date_time"]
        createdBy <- map["created_by"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        images <- map["images"]
    }
}



