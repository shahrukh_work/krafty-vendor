//
//  User.swift
//  Krafty-Vendor
//
//  Created by Mian Faizan Nasir on 6/8/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper
import SocketIO

class User {
    static let shared = User()
    var profile = Mapper<Profile>().map(JSON: [:])
    var socket:SocketIOClient!
    var mapsController: MapsViewController?
}
