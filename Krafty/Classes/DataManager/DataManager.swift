//
//  DataManager.swift
//  ShaeFoodDairy
//
//  Created by Mac on 01/04/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import ObjectMapper

class DataManager {
    
    static let shared = DataManager()
    
    func setUser (user: String) {
        UserDefaults.standard.set(user, forKey: "user_data")
    }
    
//    func getUser() -> User? {
//        var user: User?
//
//        if UserDefaults.standard.object(forKey: "user_data") != nil {
//            user = Mapper<User>().map(JSONString:UserDefaults.standard.string(forKey: "user_data")!)
//        }
//        return user
//    }
    
    func deleteUser () {
         UserDefaults.standard.set(nil, forKey: "user_data")
    }
    
    func setAuthentication (auth: String) {
        UserDefaults.standard.set(auth, forKey: "auth_data")
    }
    
//    func getAuthentication() -> AuthLogin? {
//        var auth: AuthLogin?
//        
//        if UserDefaults.standard.object(forKey: "auth_data") != nil {
//            auth = Mapper<AuthLogin>().map(JSONString:UserDefaults.standard.string(forKey: "auth_data")!)
//        }
//        return auth
//    }
    
    func deleteAuthentication () {
        UserDefaults.standard.set(nil, forKey: "auth_data")
    }
    
}
